# Проект Тераграф. Библиотека для взаимодействия HOST систем с Graph Processor Core (XRT Runtime версия), используются аппаратные FIFO буферы по 512 слов и память External Memory 


## Состав проекта

Библиотека состоит из двух взаимодействующих частей:

- Библиотека, функционирующая на хост системе x86 (host). Проект создан в IDE Eclipse. 
- Библиотека, работающа на микропроцессоре RiscV в составе Graph Rocessor Core (sw_kernel). 

## Установка

```git clone https://gitlab.com/alexpopov366/leonhard-x64.git```

## Сборка

- Библиотека host. Для сборки требуется установка библиотеки [xrt](https://gitlab.com/xilinx4jet/XRT).

```cd ./host/Hardware
   make
```

- Библиотека sw_kernel. Для сборки требуется установка [riscv toolchain](https://gitlab.com/quantr/toolchain/riscv-gnu-toolchain) и библиотеки [picolib](https://github.com/picolibc/picolibc).

```cd ./swkernel
   make
```

## Запуск проекта

```
./Hardware/Leonhardx64_OCL <Путь к xclbin файлу> <Путь к rawbinary файлу библиотеки sw_kernel>
```
