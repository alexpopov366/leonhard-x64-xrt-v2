

#include <iostream>
#include <stdio.h>
#include <stdexcept>
#include <iomanip>
#ifdef _WINDOWS
#include <io.h>
#else
#include <unistd.h>
#include <sys/time.h>
#endif


#include "experimental/xrt_device.h"
#include "experimental/xrt_kernel.h"
#include "experimental/xrt_bo.h"
#include "experimental/xrt_ini.h"

#include "gpc_defs.h"
#include "leonhardx64_xrt.h"
#include "gpc_handlers.h"

static void usage()
{
	std::cout << "usage: <xclbin> <sw_kernel>\n\n";
}

static void print_table(std::string test, float value, std::string units)
{
	std::cout << std::left << std::setfill(' ') << std::setw(50) << test << std::right << std::setw(20) << std::fixed << std::setprecision(0) << value << std::setw(15) << units << std::endl;
	std::cout << std::setfill('-') << std::setw(85) << "-" << std::endl;
}


int main(int argc, char** argv)
{

	unsigned int err = 0;
	unsigned int cores_count = 0;
	float LNH_CLOCKS_PER_SEC;

	__foreach_core(group, core) cores_count++;

	//Assign xclbin
	if (argc < 3) {
		usage();
		throw std::runtime_error("FAILED_TEST\nNo xclbin specified");
	}

	//Open device #0
	leonhardx64 lnh_inst = leonhardx64(0,argv[1]);
	__foreach_core(group, core)
	{
		lnh_inst.load_sw_kernel(argv[2], group, core);
	}

	/*
	 *
	 * SW Kernel Version and Status
	 *
	 */
	__foreach_core(group, core)
	{
		printf("Group #%d \tCore #%d\n", group, core);
		lnh_inst.gpc[group][core]->start_sync(__event__(get_version));
		printf("\tSoftware Kernel Version:\t0x%08x\n", lnh_inst.gpc[group][core]->mq_receive());
		lnh_inst.gpc[group][core]->start_sync(__event__(get_lnh_status_high));
		printf("\tLeonhard Status Register:\t0x%08x", lnh_inst.gpc[group][core]->mq_receive());
		lnh_inst.gpc[group][core]->start_sync(__event__(get_lnh_status_low));
		printf("_%08x\n", lnh_inst.gpc[group][core]->mq_receive());
	}


	//-------------------------------------------------------------
	// Измерение производительности Leonhard
	//-------------------------------------------------------------

	float interval;
	char buf[100];
	err = 0;

	time_t now = time(0);
	strftime(buf, 100, "Start at local date: %d.%m.%Y.; local time: %H.%M.%S", localtime(&now));

	printf("\nDISC system speed test v3.0\n%s\n\n", buf);
	std::cout << std::left << std::setw(50) << "Test" << std::right << std::setw(20) << "value" << std::setw(15) << "units" << std::endl;
	std::cout << std::setfill('-') << std::setw(85) << "-" << std::endl;
	print_table("Graph Processing Cores count (GPCC)", cores_count, "instances");




	/*
	 *
	 * GPC frequency measurement for the first kernel
	 *
	 */
	lnh_inst.gpc[0][LNH_CORES_LOW[0]]->start_async(__event__(frequency_measurement));

	// Measurement Body
	lnh_inst.gpc[0][LNH_CORES_LOW[0]]->sync_with_gpc(); // Start measurement
	sleep(1);
	lnh_inst.gpc[0][LNH_CORES_LOW[0]]->sync_with_gpc(); // Start measurement
	// End Body
	lnh_inst.gpc[0][LNH_CORES_LOW[0]]->finish();
	LNH_CLOCKS_PER_SEC = (float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive();
	print_table("Leonhard clock frequency (LNH_CF)", LNH_CLOCKS_PER_SEC / 1000000, "MHz");


	/*
	 *
	 * Memory write rate
	 *
	 */
	lnh_inst.gpc[0][LNH_CORES_LOW[0]]->start_sync(__event__(test0));
	interval = ((float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / LNH_CLOCKS_PER_SEC;
	//printf("Average RISCV memory write rate (AMWR64): %.1f (ops./sec.)\n", (float)1000000 / interval);
	print_table("Average RISCV memory write rate (AMWR64)", (float)1000000 / interval, "ops./sec.");

	/*
	 *
	 * Bus write rate
	 *
	 */
	lnh_inst.gpc[0][LNH_CORES_LOW[0]]->start_sync(__event__(test1));
	interval = ((float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / LNH_CLOCKS_PER_SEC;
	print_table("Average RISCV AXL bus write rate (ABWR64)",(float)1000000 / interval , "ops./sec.");


	/*
	 *
	 * Kernel RUN rate
	 *
	 */

	clock_t start,stop;

	start=clock();
	for (int i=0; i<1000; i++)
		__foreach_core(group, core)
			lnh_inst.gpc[group][core]->start_async(0);
	stop=clock();
	print_table("Average sw_kernel Run Rate (AKRR)", float(1000 * CLOCKS_PER_SEC * cores_count) / float(stop-start), "calls/sec.");

	/*
	 *
	 * Write kernel register rate
	 *
	 */

	start=clock();
	for (int i=0; i<100000; i++)
		__foreach_core(group, core)
			lnh_inst.gpc[group][core]->kernel->write_register(GPC_CONFIG_OFFSET, i);
	stop=clock();
	print_table("Average GPC IO register write rate (IORW)",float(100000 * CLOCKS_PER_SEC * cores_count) / float(stop-start) , "calls/sec.");

	/*
	 *
	 * Read kernel register rate
	 *
	 */

	start=clock();
	for (int i=0; i<100000; i++)
		__foreach_core(group, core)
			lnh_inst.gpc[group][core]->kernel->read_register(GPC_CONFIG_OFFSET);
	stop=clock();
	print_table("Average GPC IO register read rate (IORR)", float(100000 * CLOCKS_PER_SEC * cores_count) / float(stop-start), "calls/sec.");


	/*
	 *
	 * Queue single messaging send and receive rates
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(mq_rate_test));
	}
	start=clock();
	for (int i=0;i<256;i++) {
		__foreach_core(group, core)
		{
			lnh_inst.gpc[group][core]->mq_send(i);
		}
	}
	stop=clock();
	print_table("Message Queue Single Send  Rate (AQSSR)", float(256 * CLOCKS_PER_SEC * cores_count) / float(stop-start), "messages/sec.");
	start=clock();
	for (int i=0;i<256;i++) {
		__foreach_core(group, core)
			{
			lnh_inst.gpc[group][core]->mq_receive();
		}
	}
	stop=clock();
	print_table("Message Queue Single Receive Rate (AQSRR)", float(256 * CLOCKS_PER_SEC * cores_count) / float(stop-start), "messages/sec.");

	/*
	 *
	 * Queue block messaging send-receive rate
	 *
	 */

	unsigned int *host2gpc_buffer[LNH_GROUPS_COUNT][LNH_MAX_CORES_IN_GROUP];
	__foreach_core(group, core)
	{
		host2gpc_buffer[group][core] = (unsigned int*) malloc(256*sizeof(int));
		for (int i=0;i<256;i++) host2gpc_buffer[group][core][i] = i;
	}
	unsigned int *gpc2host_buffer[LNH_GROUPS_COUNT][LNH_MAX_CORES_IN_GROUP];
	__foreach_core(group, core)
	{
		gpc2host_buffer[group][core] = (unsigned int*) malloc(256*sizeof(int));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(mq_rate_test));
	}
	start=clock();
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->mq_send(256*sizeof(int),host2gpc_buffer[group][core]);
	}
	__foreach_core(group, core)
		{
		lnh_inst.gpc[group][core]->mq_receive(256*sizeof(int),gpc2host_buffer[group][core]);
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->mq_send_join();
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->mq_receive_join();
	}
	stop=clock();
	__foreach_core(group, core) {
		free(host2gpc_buffer[group][core]);
		free(gpc2host_buffer[group][core]);
	}
	print_table("Message Queue Block Send-Receive Rate (AQBSRR)", float(256 * CLOCKS_PER_SEC * cores_count) / float(stop-start), "messages/sec.");

	/*
	 *
	 * Queue single messaging send-receive rate
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(mq_rate_test));
	}
	start=clock();
	for (int i=0;i<256;i++) {
		__foreach_core(group, core)
		{
			lnh_inst.gpc[group][core]->mq_send(i);
		}
		__foreach_core(group, core)
			{
			lnh_inst.gpc[group][core]->mq_receive();
		}
	}
	stop=clock();
	print_table("Message Queue Single Send-Receive Rate (AQSSRR)", float(256 * CLOCKS_PER_SEC * cores_count) / float(stop-start), "messages/sec.");



	/*
	 *
	 * Global Memory Buffer block data transfer rate
	 *
	 */

	__foreach_core(group, core)
	{
		host2gpc_buffer[group][core] = (unsigned int*) malloc(1024*sizeof(int));
		for (int i=0;i<1024;i++) host2gpc_buffer[group][core][i] = i;
	}
	__foreach_core(group, core)
	{
		gpc2host_buffer[group][core] = (unsigned int*) malloc(1024*sizeof(int));
	}
	__foreach_core(group, core) {
		lnh_inst.gpc[group][core]->start_async(__event__(buf_rate_test));
	}
	start=clock();
	__foreach_core(group, core) {
		lnh_inst.gpc[group][core]->buf_write(1024*sizeof(int),host2gpc_buffer[group][core]);
	}
	__foreach_core(group, core) {
		lnh_inst.gpc[group][core]->buf_write_join();
	}
	__foreach_core(group, core) {
		lnh_inst.gpc[group][core]->mq_send(1024*sizeof(int));
	}
	__foreach_core(group, core) {
		lnh_inst.gpc[group][core]->mq_receive();
	}
	__foreach_core(group, core) {
		lnh_inst.gpc[group][core]->buf_read(1024*sizeof(int),gpc2host_buffer[group][core]);
	}
	__foreach_core(group, core) {
		lnh_inst.gpc[group][core]->buf_read_join();
	}
	stop=clock();
	__foreach_core(group, core) {
		free(host2gpc_buffer[group][core]);
		free(gpc2host_buffer[group][core]);
	}
	print_table("Global Memory Send-Receive Rate (GMSRR)", float(4096*CLOCKS_PER_SEC*cores_count) / float(stop-start), "bytes/sec.");

	/*
	 *
	 * External Memory block data transfer rate
	 *
	 */

	char* host2gpc_ext_buffer[LNH_GROUPS_COUNT][LNH_MAX_CORES_IN_GROUP];

	__foreach_core(group, core)
	{
		host2gpc_ext_buffer[group][core] = lnh_inst.gpc[group][core]->external_memory_create_buffer(32*1024*sizeof(int));
		for (int i=0;i<1024;i++) ((unsigned int*)host2gpc_ext_buffer[group][core])[i] = i;
		for (int i=1024;i<2048;i++) ((unsigned int*)host2gpc_ext_buffer[group][core])[i] = 0; //required by XRT to avoid read-before_write exceprion
		lnh_inst.gpc[group][core]->external_memory_sync_to_device(0,2*1024*sizeof(int));
	}
	__foreach_core(group, core) {
		lnh_inst.gpc[group][core]->start_async(__event__(external_memory_test));
	}
	start=clock();
	__foreach_core(group, core) {
		long long tmp = lnh_inst.gpc[group][core]->external_memory_address();
		lnh_inst.gpc[group][core]->mq_send((unsigned int)tmp);
	}
	__foreach_core(group, core) {
		lnh_inst.gpc[group][core]->mq_send(1024*sizeof(int));
	}
	__foreach_core(group, core) {
		lnh_inst.gpc[group][core]->mq_receive();
	}
	__foreach_core(group, core) {
		lnh_inst.gpc[group][core]->external_memory_sync_from_device(1024*sizeof(int),1024*sizeof(int));
	}
	stop=clock();
	__foreach_core(group, core) {
		free(host2gpc_ext_buffer[group][core]);
	}
	print_table("External Memory Send-Receive Rate (EMSRR)", float(4096*CLOCKS_PER_SEC*cores_count) / float(stop-start), "bytes/sec.");


	/*
	 *
	 * Average time of the sequential insert
	 *
	 */
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(test2));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	print_table("Average sequential insert rate (ASIR64)", (float)1000000 / interval, "ops./sec.");

	/*
	 *
	 * Average time of the random insert
	 *
	 */
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(test3));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	print_table("Average random insert rate (ARIR64)", (float)1000000 / interval, "ops./sec.");

	/*
	 *
	 * Sequential search
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(test4));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	print_table("Average sequential search rate (ASSR64)", (float)1000000 / interval, "ops./sec.");

	/*
	 *
	 * Random search
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(test5));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	print_table("Average random search rate (ARSR64)", (float)1000000 / interval, "ops./sec.");

	/*
	 *
	 * Average sequential delete rate
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(test6));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	print_table("Average sequential delete rate (ASDR64)", (float)1000000 / interval, "ops./sec.");

	/*
	 *
	 * Average random delete rate
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(test7));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	print_table("Average random delete rate (ARDR64)", (float)1000000 / interval, "ops./sec.");

	/*
	 *
	 * Local memory 1M keys traversing time
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(test8));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	print_table("LSM 1M keys traversing time (LSMTT64)", 1000 * interval, "msec.");

	/*
	 *
	 * Average neighbours search rate
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(test9));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	print_table("Average neighbours search rate (ANSR64)", (float)1000000 / interval, "ops./sec.");

	/*
	 *
	 * 1M dataset AND time
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(test10));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	print_table("1M dataset AND time (ANDT64)", 1000 * interval, "msec.");

	/*
	 *
	 * 1M datasets OR time
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(test11));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	print_table("1M dataset OR time (ORT64)", 1000 * interval, "msec.");

	/*
	 *
	 * 1M dataset NOT time
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(test12));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	print_table("1M dataset NOT time (NOTT64)", 1000 * interval, "msec.");

	/*
	 *
	 * 1M dataset Slice time
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(test13));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	print_table("1M dataset slice time (SLCT64)", 1000 * interval, "msec.");

	/*
	 *
	 * Worst possible insert time / Full LSM shift
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(test14));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	print_table("Worst possible insert time (WPIT64)", 1000 * interval, "msec.");

	/*
	 *
	 * 1M dataset squiz time
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(test15));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	print_table("1M dataset squiz rate (SQZT64)", interval, "sec.");


	printf("\nExperiments:\n\n");
	/*
	 *
	 * Experiment 1
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(exp1));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
		printf("Test #1. Error count:                 	%d\n", lnh_inst.gpc[group][core]->mq_receive());
	}

	/*
	 *
	 * Experiment 2
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(exp2));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
		printf("Test #2. Error count:                 	%d\n", lnh_inst.gpc[group][core]->mq_receive());
	}

	/*
	 *
	 * Experiment 3
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(exp3));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
		printf("Test #3. Error count:                 	%d\n", lnh_inst.gpc[group][core]->mq_receive());
	}

	/*
	 *
	 * Experiment 4
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(exp4));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
		printf("Test #4. Error count:                 	%d\n", lnh_inst.gpc[group][core]->mq_receive());
	}

	/*
	 *
	 * Experiment 5
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(exp5));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
		printf("Test #5. Error count:                 	%d\n", lnh_inst.gpc[group][core]->mq_receive());
	}

	/*
	 *
	 * Experiment 5
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(exp5));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
		printf("Test #5. Error count:                 	%d\n", lnh_inst.gpc[group][core]->mq_receive());
	}

	/*
	 *
	 * Experiment 6
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(exp6));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
		printf("Test #6. Error count:                 	%d\n", lnh_inst.gpc[group][core]->mq_receive());
	}

	/*
	 *
	 * Experiment 7
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(exp7));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
		printf("Test #7. Error count:                 	%d\n", lnh_inst.gpc[group][core]->mq_receive());
	}

	/*
	 *
	 * Experiment 8
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(exp8));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
		printf("Test #8. Error count:                 	%d\n", lnh_inst.gpc[group][core]->mq_receive());
	}

	/*
	 *
	 * Experiment 9
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(exp9));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
		printf("Test #9. Error count:                 	%d\n", lnh_inst.gpc[group][core]->mq_receive());
	}

	/*
	 *
	 * Experiment 10
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(exp10));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
		printf("Test #10. Error count:                 	%d\n", lnh_inst.gpc[group][core]->mq_receive());
	}

	now = time(0);
	strftime(buf, 100, "Stop at local date: %d.%m.%Y.; local time: %H.%M.%S", localtime(&now));
	printf("DISC system speed test v1.1\n%s\n\n", buf);

	//--------------------------------------------------------------------------
	// Shutdown and cleanup
	//--------------------------------------------------------------------------

	if (err)
	{
		printf("ERROR: Test failed\n");
		return EXIT_FAILURE;
	}
	else
	{
		printf("INFO: Test completed successfully.\n");
		return EXIT_SUCCESS;
	}





	return 0;
}
