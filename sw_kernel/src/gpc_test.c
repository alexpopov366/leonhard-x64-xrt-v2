/*
 * gpc_test.c
 *
 * sw_kernel library
 *
 *  Created on: April 23, 2021
 *      Author: A.Popov
 */

#include <stdlib.h>
#include "lnh64.h"
#include "gpc_io_swk.h"
#include "gpc_handlers.h"

#define VERSION 26
#define DEFINE_LNH_DRIVER
#define DEFINE_MQ_R2L
#define DEFINE_MQ_L2R
#define ROM_LOW_ADDR 0x00000000
#define ITERATIONS_COUNT    1
#define MEASURE_KEY_COUNT   1000000
#define __fast_recall__

extern lnh lnh_core;
extern global_memory_io gmio;
volatile unsigned int event_source;

int main(void) {
    /////////////////////////////////////////////////////////
    //                  Main Event Loop
    /////////////////////////////////////////////////////////
    //Leonhard driver structure should be initialised
    lnh_init();
    //Initialise host2gpc and gpc2host queues
    gmio_init(lnh_core.partition.data_partition);
    for (;;) {
        //Wait for event
        while (!gpc_start());
        //Enable RW operations
        set_gpc_state(BUSY);
        //Wait for event
        event_source = gpc_config();
        switch(event_source) {
            /////////////////////////////////////////////
            //  Measure GPN operation frequency
            /////////////////////////////////////////////
            case __event__(frequency_measurement) : frequency_measurement(); break;
            case __event__(test0) : test0(); break;
            case __event__(test1) : test1(); break;
            case __event__(test2) : test2(); break;
            case __event__(test3) : test3(); break;
            case __event__(test4) : test4(); break;
            case __event__(test5) : test5(); break;
            case __event__(test6) : test6(); break;
            case __event__(test7) : test7(); break;
            case __event__(test8) : test8(); break;
            case __event__(test9) : test9(); break;
            case __event__(test10) : test10(); break;
            case __event__(test11) : test11(); break;
            case __event__(test12) : test12(); break;
            case __event__(test13) : test13(); break;
            case __event__(test14) : test14(); break;
            case __event__(test15)  : test15(); break;
            case __event__(exp1) : exp1(); break;
            case __event__(exp2) : exp2(); break;
            case __event__(exp3) : exp3(); break;
            case __event__(exp4) : exp4(); break;
            case __event__(exp5) : exp5(); break;
            case __event__(exp6) : exp6(); break;
            case __event__(exp7) : exp7(); break;
            case __event__(exp8) : exp8(); break;
            case __event__(exp9) : exp9(); break;
            case __event__(exp10) : exp10(); break;
            case __event__(get_lnh_status_low) : get_lnh_status_low(); break;
            case __event__(get_lnh_status_high) : get_lnh_status_high(); break;
            case __event__(get_version): get_version(); break;
            case __event__(mq_rate_test): mq_rate_test(); break;
            case __event__(buf_rate_test): buf_rate_test(); break;
            case __event__(external_memory_test): external_memory_test(); break;
        }
        //Disable RW operations
        set_gpc_state(IDLE);
        while (gpc_start());

    }
}
    
//-------------------------------------------------------------
//      Глобальные переменные (для сокращения объема кода)
//-------------------------------------------------------------
    
        u32 LNH_key;
        u32 LNH_value;
        u32 LNH_status;
        u64 TSC_start;
        u64 TSC_stop;
        u32 interval;
        int i,j;
        u32 err=0;
        u32 k10,k20,k11,k21;
        u32 crc10,crc20,crc11,crc21;
        u32 low,k;
        u32 num,num1;
        u32 jl;    

     

//-------------------------------------------------------------
//      Измерение тактовой частоты GPN
//-------------------------------------------------------------
 
void frequency_measurement() {
    
        sync_with_host();
        lnh_sw_reset();
        lnh_rd_reg32_byref(TSC_LOW,&TSC_start);
        sync_with_host();
        lnh_rd_reg32_byref(TSC_LOW,&TSC_stop);
        interval = TSC_stop-TSC_start;
        mq_send(interval);

}


//-------------------------------------------------------------
//      Получить версию микрокода 
//-------------------------------------------------------------
 
void get_version() {
    
        mq_send(VERSION);

}
   

//-------------------------------------------------------------
//      Получить регистр статуса LOW Leonhard 
//-------------------------------------------------------------
 
void get_lnh_status_low() {
    
        lnh_rd_reg32_byref(LNH_STATE_LOW,&lnh_core.result.status);
        mq_send(lnh_core.result.status);

}

//-------------------------------------------------------------
//      Получить регистр статуса HIGH Leonhard 
//-------------------------------------------------------------
 
void get_lnh_status_high() {
    
        lnh_rd_reg32_byref(LNH_STATE_HIGH,&lnh_core.result.status);
        mq_send(lnh_core.result.status);

}

//-------------------------------------------------------------
//      Измерение скорости обмена сообщениями через очереди MQ
//-------------------------------------------------------------

void mq_rate_test() {

    for (int i=0;i<256;i++) 
        mq_send(mq_receive()); //echo

    
}


//-------------------------------------------------------------
//      Измерение скорости обмена данными через буферы 4К
//-------------------------------------------------------------

void buf_rate_test() {
    unsigned int size = mq_receive();
    unsigned int *buffer = (unsigned int*)malloc(size);
    buf_read(size, buffer);
    buf_write(size, buffer);   
    mq_send(size);
    free(buffer);
}



//-------------------------------------------------------------
//      Измерение скорости обмена данными с external memory
//-------------------------------------------------------------

void external_memory_test() {
    unsigned int buffer_pointer = mq_receive();
    unsigned int size = mq_receive();
    unsigned int *buffer = (unsigned int*)malloc(size);
    ext_buf_read(size, (unsigned int *)buffer_pointer, buffer);
    ext_buf_write(size, (unsigned int *)buffer_pointer, buffer);
    mq_send(size);
    free(buffer);
}

//-------------------------------------------------------------
//      Измерение производительности Leonhard
//-------------------------------------------------------------
        

void test0 () {
        /*
        *
        * Memory write rate
        *
        */
        
        int *buf;
        buf = (int*)malloc(1024*sizeof(int));
        
        lnh_sw_reset();
        lnh_rd_reg32_byref(TSC_LOW,&TSC_start);
        i=0;
        for (LNH_key=0;LNH_key<MEASURE_KEY_COUNT;LNH_key++) {
            buf[i]=LNH_key;
            if (i++>=1024) i=0;
        }
        lnh_rd_reg32_byref(TSC_LOW,&TSC_stop);
        
        interval = TSC_stop-TSC_start;
        mq_send(interval);
        
        free(buf);

}

void test1 () {
        /*
        *
        * Bus write rate
        *
        */

        lnh_sw_reset();
        lnh_rd_reg32_byref(TSC_LOW,&TSC_start);

        for (LNH_key=0;LNH_key<MEASURE_KEY_COUNT;LNH_key++) {

            lnh_wr_reg32_byval(KEY2LNH_LOW,LNH_key);
        }
        
        lnh_rd_reg32_byref(TSC_LOW,&TSC_stop);
        
        interval = TSC_stop-TSC_start;
        mq_send(interval); 

}

void test2 () {
        /*
        *
        * Average time of the sequential insert
        *
        */

        lnh_sw_reset();
        lnh_rd_reg32_byref(TSC_LOW,&TSC_start);

        #ifdef __fast_recall__
            for (
                /*init   */ LNH_key=0, lnh_ins_async(1,LNH_key,LNH_key), lnh_ins_async(1,MEASURE_KEY_COUNT-1,MEASURE_KEY_COUNT-1),LNH_key++; 
                /*limit  */ LNH_key<MEASURE_KEY_COUNT-1; 
                /*iterate*/ LNH_key++
                ) 
                lnh_fast_recall(LNH_key,LNH_key);
        #else        
            for (LNH_key=0;LNH_key<MEASURE_KEY_COUNT;LNH_key++)
                //lnh_ins_syncm(mr0,1,LNH_key,LNH_key);
                lnh_ins_async(1,LNH_key,LNH_key);
                //lnh_ins_sync(1,LNH_key,LNH_key);
        #endif
        
        lnh_sync();
        
        lnh_rd_reg32_byref(TSC_LOW,&TSC_stop);
        interval = TSC_stop-TSC_start;
        mq_send(interval); 

}

void test3 () {

	    /*
	     *
	     * Average time of the random insert
	     *
	     */

        lnh_sw_reset();
        lnh_rd_reg32_byref(TSC_LOW,&TSC_start);

        #ifdef __fast_recall__
            for (
            /*init   */ i=0, LNH_key=rand_single(), lnh_ins_async(1,LNH_key,LNH_key);
            /*limit  */ i<MEASURE_KEY_COUNT;
            /*iterate*/ i++
                ) 
            {
                LNH_key=rand_single();
                lnh_fast_recall(LNH_key,LNH_key);
            }
        #else        
            for (i=0;i<MEASURE_KEY_COUNT-1;i++) {
                LNH_key=rand_single();
                //lnh_ins_syncm(mr0,1,LNH_key,LNH_key);
                lnh_ins_async(1,LNH_key,LNH_key);
                //lnh_ins_sync(1,LNH_key,LNH_key);
            }
        #endif
	    lnh_sync();
        
        lnh_rd_reg32_byref(TSC_LOW,&TSC_stop);
        interval = TSC_stop-TSC_start;
        mq_send(interval); 

}

void test4 () {
        
	    /*
	     *
	     * Sequential search
	     *
	     */

        lnh_sw_reset();
        for (LNH_key=0;LNH_key<MEASURE_KEY_COUNT;LNH_key++)
	        	lnh_ins_async(1,LNH_key,LNH_key);
    	lnh_sync();

	    lnh_rd_reg32_byref(TSC_LOW,&TSC_start);

        #ifdef __fast_recall__
            for (
            /*init   */ LNH_key=0,lnh_searchq(1,LNH_key);
            /*limit  */ LNH_key<MEASURE_KEY_COUNT;
            /*iterate*/ LNH_key++
                ) 
                lnh_fast_recall(LNH_key);
        #else        
            for (LNH_key=0;LNH_key<MEASURE_KEY_COUNT-1;LNH_key++) 
                lnh_searchm(mr0,mr1,mr2,1,LNH_key);
                //lnh_search(1,LNH_key);
        #endif
        
        lnh_sync();
        
        lnh_rd_reg32_byref(TSC_LOW,&TSC_stop);
        interval = TSC_stop-TSC_start;
        mq_send(interval); 

}

void test5 () {

	    /*
	     *
	     * Random search
	     *
	     */

        lnh_sw_reset();

        for (LNH_key=0;LNH_key<MEASURE_KEY_COUNT;LNH_key++) 
	        	lnh_ins_async(1,LNH_key,LNH_key);
	    lnh_sync();
        
	    lnh_rd_reg32_byref(TSC_LOW,&TSC_start);

        #ifdef __fast_recall__
            for (
            /*init   */ i=1, LNH_key=rand_single()%MEASURE_KEY_COUNT,lnh_searchq(1,LNH_key);
            /*limit  */ i<MEASURE_KEY_COUNT;
            /*iterate*/ i++
                ) 
            {
                LNH_key=rand_single()%MEASURE_KEY_COUNT;
                lnh_fast_recall(LNH_key);
            }
        #else        
            for (i=0;i<MEASURE_KEY_COUNT;i++) {
                LNH_key=rand_single()%MEASURE_KEY_COUNT;
                lnh_searchm(mr0,mr1,mr2,1,LNH_key);
                //lnh_search(1,LNH_key);
            }
        #endif
        
        lnh_sync();
        
        lnh_rd_reg32_byref(TSC_LOW,&TSC_stop);
        interval = TSC_stop-TSC_start;
        mq_send(interval); 

}

void test6 () {

        /*
	     *
	     * Average sequential delete rate
	     *
	     */

        lnh_sw_reset();

        for (LNH_key=0;LNH_key<MEASURE_KEY_COUNT;LNH_key++) 
            lnh_ins_async(1,LNH_key,LNH_key);
	    
        lnh_sync();
        
	    lnh_rd_reg32_byref(TSC_LOW,&TSC_start);
        
        
        #ifdef __fast_recall__
            for (
            /*init   */ LNH_key=0,lnh_del_async(1,LNH_key),LNH_key++;
            /*limit  */ LNH_key<MEASURE_KEY_COUNT;
            /*iterate*/ LNH_key++
                ) 
            {
                lnh_fast_recall(LNH_key);
            }
        #else        
            for (LNH_key=0;LNH_key<MEASURE_KEY_COUNT-1;LNH_key++) {
                lnh_del_async(1,LNH_key);
                //lnh_del_sync(1,LNH_key);
                //lnh_del_syncm(mr0,1,LNH_key);
            }
        #endif        

        lnh_sync();
        
        lnh_rd_reg32_byref(TSC_LOW,&TSC_stop);
        interval = TSC_stop-TSC_start;
        mq_send(interval); 

}

void test7 () {

        /*
	     *
	     * Average random delete rate
	     *
	     */

        lnh_sw_reset();

        for (LNH_key=0;LNH_key<10*MEASURE_KEY_COUNT;LNH_key++)
        	lnh_ins_async(1,LNH_key,LNH_key);
        lnh_sync();
        
	    lnh_rd_reg32_byref(TSC_LOW,&TSC_start);

        #ifdef __fast_recall__
            for (
            /*init   */ i=0,LNH_key=rand_single()%MEASURE_KEY_COUNT,lnh_del_async(1,LNH_key);
            /*limit  */ i<MEASURE_KEY_COUNT;
            /*iterate*/ i++
                ) 
            {
                LNH_key=rand_single()%MEASURE_KEY_COUNT;
                lnh_fast_recall(LNH_key);
            }
        #else        
            for (i=0;i<MEASURE_KEY_COUNT;i++) {
                LNH_key=rand_single()%MEASURE_KEY_COUNT;
                lnh_del_async(1,LNH_key);
                //lnh_del_sync(1,LNH_key);
                //lnh_del_syncm(mr0,1,LNH_key);
            }
        #endif  
        
        lnh_sync();
        
        lnh_rd_reg32_byref(TSC_LOW,&TSC_stop);
        interval = TSC_stop-TSC_start;
        mq_send(interval); 
        
}

void test8 () {

	    /*
	     *
	     * Local memory 1M keys traversing time
	     *
	     */
        lnh_sw_reset();
        
        for (LNH_key=0;LNH_key<MEASURE_KEY_COUNT;LNH_key++) 
        	lnh_ins_async(1,LNH_key,LNH_key);
        lnh_sync();

        lnh_rd_reg32_byref(TSC_LOW,&TSC_start);
        
        #ifdef __fast_recall__
            for (
            /*init   */ LNH_key=0,lnh_nextm(mr0,mr1,mr2,1,LNH_key),LNH_key++;
            /*limit  */ LNH_key<MEASURE_KEY_COUNT;
            /*iterate*/ LNH_key++
                ) 
            {
                lnh_fast_recall(LNH_key);
            }
        #else        
            for (LNH_key=0;LNH_key<MEASURE_KEY_COUNT;LNH_key++) 
                lnh_nextm(mr0,mr1,mr2,1,LNH_key);
                //lnh_next(1,LNH_key);            
        #endif         

        lnh_sync();
        
        lnh_rd_reg32_byref(TSC_LOW,&TSC_stop);
        interval = TSC_stop-TSC_start;
        mq_send(interval); 
}

void test9 () {


	    /*
	     *
	     * Average neighbours search rate
	     *
	     */

        lnh_sw_reset();

        for (LNH_key=1;LNH_key<=2000001;LNH_key+=2) 
        	lnh_ins_async(1,LNH_key,LNH_key);
        lnh_sync();
       
	    lnh_rd_reg32_byref(TSC_LOW,&TSC_start);

        #ifdef __fast_recall__
            for (
            /*init   */ i=0,LNH_key=((rand_single()%1000000)<<1),lnh_ngrm(mr0,mr1,mr2,1,LNH_key);
            /*limit  */ i<MEASURE_KEY_COUNT;
            /*iterate*/ i++
                ) 
            {
                LNH_key=(rand_single()%1000000)<<1;
                lnh_fast_recall(LNH_key);
            }
        #else        
            for (i=0;i<MEASURE_KEY_COUNT;i++) {
                LNH_key=((rand_single()%1000000)<<1);
                lnh_ngrm(mr0,mr1,mr2,1,LNH_key);
                //lnh_ngr(1,LNH_key);  
            }
        #endif 
            
        lnh_sync();
        
        lnh_rd_reg32_byref(TSC_LOW,&TSC_stop);
        interval = TSC_stop-TSC_start;
        mq_send(interval); 
}

void test10 () {

	    /*
	     *
	     * 1M dataset AND time
	     *
	     */

        lnh_sw_reset();

 	    for (LNH_key=0;LNH_key<1000000;LNH_key++) 
            lnh_ins_async(1,LNH_key,LNH_key);

	    for (LNH_key=0;LNH_key<1000000;LNH_key++) 
        	lnh_ins_async(2,LNH_key,LNH_key);
        
        lnh_sync();

        lnh_rd_reg32_byref(TSC_LOW,&TSC_start);

    	lnh_and_sync(1,2,3);
        
        lnh_rd_reg32_byref(TSC_LOW,&TSC_stop);
        interval = TSC_stop-TSC_start;
        mq_send(interval); 
}

void test11 () {
        

	    /*
	     *
	     * 1M datasets OR time
	     *
	     */

        lnh_sw_reset();

 	    for (LNH_key=0;LNH_key<1000000;LNH_key++) 
            lnh_ins_async(1,LNH_key,LNH_key);

	    for (LNH_key=0;LNH_key<1000000;LNH_key++) 
        	lnh_ins_async(2,LNH_key,LNH_key);
        
        lnh_sync();

        lnh_rd_reg32_byref(TSC_LOW,&TSC_start);

    	lnh_or_sync(1,2,3);
        
        lnh_rd_reg32_byref(TSC_LOW,&TSC_stop);
        interval = TSC_stop-TSC_start;
        mq_send(interval); 
}

void test12 () {

	    /*
	     *
	     * 1M dataset NOT time
	     *
	     */

        lnh_sw_reset();

 	    for (LNH_key=1;LNH_key<2000000;LNH_key=LNH_key+2) 
            lnh_ins_async(1,LNH_key,LNH_key);

	    for (LNH_key=0;LNH_key<2000000;LNH_key=LNH_key+2) 
        	lnh_ins_async(2,LNH_key,LNH_key);
        
        lnh_sync();

        lnh_rd_reg32_byref(TSC_LOW,&TSC_start);

    	lnh_not_sync(1,2,3);
        
        lnh_rd_reg32_byref(TSC_LOW,&TSC_stop);
        interval = TSC_stop-TSC_start;
        mq_send(interval); 
}

void test13 () {

	    /*
	     *
	     * 1M dataset Slice time
	     *
	     */
        lnh_sw_reset();

 	    for (LNH_key=0;LNH_key<2000000;LNH_key++) 
            lnh_ins_async(1,LNH_key,LNH_key);

 	    LNH_key=1000000;
        lnh_sync();

        lnh_rd_reg32_byref(TSC_LOW,&TSC_start);

        lnh_greq_sync(LNH_key,1,2);
        
        lnh_rd_reg32_byref(TSC_LOW,&TSC_stop);
        interval = TSC_stop-TSC_start;
        mq_send(interval); 
}

void test14 () {

 	    /*
	     *
	     * Worst possible insert time / Full LSM shift
	     *
	     */

        lnh_sw_reset();

 	    for (LNH_key=1;LNH_key<=max_keys_count;LNH_key++) 
            lnh_ins_async(1,LNH_key,LNH_key);

 	    LNH_key=max_keys_count;
        lnh_del_async(1,LNH_key);
        lnh_sync();

        lnh_rd_reg32_byref(TSC_LOW,&TSC_start);

 	    LNH_key=0;
	    lnh_ins_sync(1,LNH_key,LNH_key);

        lnh_rd_reg32_byref(TSC_LOW,&TSC_stop);
        interval = TSC_stop-TSC_start;
        mq_send(interval); 
}

void test15 () {

	    /*
	     *
	     * 1M dataset squiz time
	     *
	     */
        lnh_sw_reset();

	    for (i=0;i<1000000;i++) {
	        LNH_key=rand_single();
	        lnh_ins_async(1,LNH_key,LNH_key);
	    }        
        lnh_sync();
        
	    lnh_rd_reg32_byref(TSC_LOW,&TSC_start);

        lnh_sq_sync(1);

        lnh_rd_reg32_byref(TSC_LOW,&TSC_stop);
        interval = TSC_stop-TSC_start;
        mq_send(interval); 

}



void exp1() {

	    //-------------------------------------------------------------
	    // тест вставки и поиска
	    //-------------------------------------------------------------

        err=0; 
        
        lnh_sw_reset();

        
        {
	        for (i = 0; i<ITERATIONS_COUNT; i++) {

                #define MAX_KEY 7*8*8*8*8*8*8*8*4
                
                for (
                /*init   */ LNH_key=0,lnh_ins_async(1,LNH_key,LNH_key),lnh_ins_async(1,MAX_KEY-1,MAX_KEY-1),LNH_key++;
                /*limit  */ LNH_key<MAX_KEY-1;
                /*iterate*/ LNH_key++
                    )
                {
                	lnh_fast_recall(LNH_key,LNH_key);
	            }

	            lnh_sq_sync(1);

                for (
                /*init   */ LNH_key=0,lnh_ins_async(2,LNH_key,LNH_key),lnh_ins_async(2,MAX_KEY-1,MAX_KEY-1),LNH_key++;
                /*limit  */ LNH_key<MAX_KEY-1;
                /*iterate*/ LNH_key++
                    )
                {
                	lnh_fast_recall(LNH_key,LNH_key);
	            }

	            
            	lnh_sync();

                for (int key=0; key<MAX_KEY; key++) {
	                LNH_key=key;
                	if ( ! (lnh_search(1,LNH_key)) ) {
                    	err++;
	            	}
	            }

	            for (int key=0; key<MAX_KEY; key++) {
	                LNH_key=key;
                	if ( ! (lnh_search(2,LNH_key)) ) {
                    	err++;
	                }
	            }

            	lnh_del_str_sync(1);
            	lnh_del_str_sync(2);

	        }
	    }	    
        mq_send(err);
}


void exp2 () {


	    //-------------------------------------------------------------
	    // тест вставки всей памяти, удаления, сдвига всей памяти
	    //-------------------------------------------------------------
        
	    {
            
            err=0;

	        LNH_value=0;
	        for (i = 0; i<ITERATIONS_COUNT; i++) {

	            for (LNH_key=1;LNH_key<=max_keys_count;LNH_key++) {
                	if ( ! lnh_ins_async(1,LNH_key,LNH_value) ) {
                    	err++;
                	}
	            }
	            LNH_key=max_keys_count;
            	if ( ! lnh_del_async(1,LNH_key) ) {
                	err++;
	            }
	            LNH_key=0;
            	if ( ! lnh_ins_async(1,LNH_key,LNH_value) ) {
                	err++;
	            }

	            for (j=0;j<max_keys_count;j++) {
	                LNH_key=j;
                	if ( ! lnh_search(1,LNH_key) ) {
                    	err++;
	                }
	            }

            	lnh_del_str_sync(1);

                
            }
	    }
        
        mq_send(err);
	    
}

void exp3 () {

        //-------------------------------------------------------------
	    // Тест nsm и ngr
	    //-------------------------------------------------------------

	    {
            
            err=0;

	        for (i = 0; i<ITERATIONS_COUNT; i++) {

	            for (j=1; j<1000000;j=j+2) {
	                LNH_key=j;
                	if ( ! lnh_ins_async(1,LNH_key,LNH_key) ) {
                    	err++;
	                }
	            }

	            for (j=1;j<1000000;j=j+2) {
	                LNH_key=j;
                	if ( ! lnh_search(1,LNH_key) ) {
                    	err++;
	                }
	            }

	            for (j=2; j<=1000000;j=j+2) {
	                LNH_key=j;
                	if ( (! lnh_nsm(1,LNH_key)) || (lnh_core.result.key != (j-1)) ) {
                    	lnh_nsm(1,LNH_key);
                    	err++;
	                }
	            }
	            for (j=3; j<1000000;j=j+2) {
	                LNH_key=j;
                	if ( (! lnh_nsm(1,LNH_key)) || (lnh_core.result.key != (j-2)) ) {
                    	lnh_nsm(1,LNH_key);
                    	err++;
	                }
	            }
	            for (j=0; j<1000000;j=j+2) {
	                LNH_key=j;
                	if ( (! lnh_ngr(1,LNH_key)) || (lnh_core.result.key != (j+1)) ) {
                    	err++;
	                }
	            }
	            for (j=1; j<999999;j=j+2) {
	                LNH_key=j;
                	if ( (! lnh_ngr(1,LNH_key)) || (lnh_core.result.key != (j+2)) ) {
                    	err++;
	                }
	            }


	            for (j=499999; j>0;j=j-2) {
	                LNH_key=j;
                	if ( !lnh_del_sync(1,LNH_key) ) {
                    	err++;
	                }
	            }
	            for (j=500001; j<1000000;j=j+2) {
	                LNH_key=j;
                	if ( !lnh_del_sync(1,LNH_key) ) {
                    	err++;
	                }
	            }

            	if (lnh_get_num(1) != 0) {
                	err++;
	            }


            	lnh_del_str_sync(1);

	        }

	    }

        mq_send(err);

}

void exp4 () {


    //-------------------------------------------------------------
    // тест на случайных данных
    //-------------------------------------------------------------

    {
        
        err=0;

        for (i = 0; i<ITERATIONS_COUNT; i++) {

        	k10=k20=k11=k21=0;
        	crc10=crc20=crc11=crc21=0;

			for (low=1000000;low<10000000;low+=1000000) { //default
					//Первая структура
					for (int j=0; j<low;j++) {
						//LNH_key=(rand()%1000000000)<<32;
						LNH_key=rand_single();
						if ( ! (lnh_search(1,LNH_key)) ) {
							crc10+=LNH_key;
							k10++;
							//if (! (lnh_ins_syncm(mr0,1,LNH_key,LNH_key)) ) {
							if (! (lnh_ins_async(1,LNH_key,LNH_key)) ) {
								err++;
							}
							lnh_sync();
						}
					}
					lnh_sync();
					k11 = lnh_get_num(1);
					lnh_get_first(1);
					crc11=lnh_core.result.key;
					while (lnh_next(1,lnh_core.result.key)) {
						k11--;
						crc11+=lnh_core.result.key;
					}
					k11--;
					if (crc10 != crc11) {
						err++;
					}
					if (lnh_get_num(1) != k10) {
						err++;
					}

					lnh_sq_sync(1);

                    //Вторая структура

					lnh_sync();
					for (int j=0; j<low;j++) {
						//LNH_key=(rand()%1000000000)<<32;
						LNH_key=rand_single();
						if ( ! (lnh_search(2,LNH_key)) ) {
							crc20+=LNH_key;
							k20++;
							if (! (lnh_ins_async(2,LNH_key,LNH_key)) ) {
								err++;
							}
							lnh_sync();
						}
					}
					lnh_sync();

					lnh_get_first(2);
					crc21=lnh_core.result.key;
					k21 = lnh_get_num(2);
					while (lnh_next(2,lnh_core.result.key)) {
						k21--;
						crc21+=lnh_core.result.key;
					}
					k21--;
					if (crc20 != crc21) {
						err++;
					}
					if (lnh_get_num(2) != k20) {
						err++;
					}

					lnh_sq_sync(2);

					for (int ij=0;ij<low;ij+=2) {
						if (lnh_ngr(1,low)) {
							crc10-=lnh_core.result.key;
							k10--;
							lnh_del_sync(1,lnh_core.result.key);
						}
					}
					for (int ij=0;ij<low;ij+=2) {
						if (lnh_ngr(2,low)) {
							crc20-=lnh_core.result.key;
							k20--;
							lnh_del_sync(2,lnh_core.result.key);
						}
					}
				}
            lnh_del_str_sync(1);
            lnh_del_str_sync(2);
        }
    }

    mq_send(err);

}

void exp5 () {


    //-------------------------------------------------------------
    // тест not
    //-------------------------------------------------------------

    {
        
        err=0;

        for (i = 0; i<ITERATIONS_COUNT; i++) {
            for (j=0; j<100000;j++) {
                LNH_key=j;
                if ( ! lnh_ins_async(1,LNH_key,LNH_key) ) {
                    err++;
                }
            }
            for (j=50000; j<150000;j++) {
                LNH_key=j;
                if ( ! lnh_ins_async(2,LNH_key,LNH_key) ) {
                    err++;
                }
            }
            lnh_not_sync(1,2,3);
            num=lnh_get_num(1);
            if (lnh_get_first(1)) k=1;
            jl=lnh_core.result.key;
            while (lnh_next(1,lnh_core.result.key)) {
                if (jl+1 != lnh_core.result.key || lnh_core.result.key>=100000)
                    err++;
                jl=lnh_core.result.key;
                k++;
            }
            num=lnh_get_num(2);
            if (lnh_get_first(2)) k=1;
            jl=lnh_core.result.key;
            while (lnh_next(2,lnh_core.result.key)) {
                if (jl+1 != lnh_core.result.key || lnh_core.result.key>=150000 || lnh_core.result.key<50000 )
                    err++;
                jl=lnh_core.result.key;
                k++;
            }
            num=lnh_get_num(3);
            if (lnh_get_first(3)) k=1;
            jl=lnh_core.result.key;
            while (lnh_next(3,lnh_core.result.key)) {
                if (jl+1 != lnh_core.result.key || lnh_core.result.key>=50000)
                    err++;
                jl=lnh_core.result.key;
                k++;
            }
            if (num!=k||num!=50000) {
                err++;
            }
            lnh_get_first(3);
            for (j=0;j<50000;j++) {
                LNH_key=j;
                if ( ! lnh_search(3,LNH_key) ) {
                    err++;
                }
            }

            lnh_del_str_sync(1);
            lnh_del_str_sync(2);
            lnh_del_str_sync(3);

        }

    }

    mq_send(err);

}

void exp6 () {

    //-------------------------------------------------------------
    // тест and
    //-------------------------------------------------------------

    {
        
        err=0;

        LNH_key=0;

        for (i = 0; i<ITERATIONS_COUNT; i++) {

            for (j=0; j<100000;j++) {
                LNH_key=j;
                if ( ! lnh_ins_async(1,LNH_key,LNH_key) ) {
                    err++;
                }
            }
            for (j=50000; j<150000;j++) {
                LNH_key=j;
                lnh_ins_async(2,LNH_key,LNH_key);
            }
            lnh_and_sync(1,2,3);
            num=lnh_get_num(3);
            if (num!=50000) {
                err++;
            }
            for (j=50000;j<100000;j++) {
                LNH_key=j;
                if ( ! lnh_search(3,LNH_key) ) {
                    err++;
                }
            }
            lnh_del_str_sync(1);
            lnh_del_str_sync(2);
            lnh_del_str_sync(3);
        }

    }
             
    mq_send(err);

}

void exp7 () {
    
    
    //-------------------------------------------------------------
    // тест or
    //-------------------------------------------------------------

    {
        err=0;

        LNH_key=0;

        for (i = 0; i<ITERATIONS_COUNT; i++) {

            for (j=0; j<100000;j++) {
                LNH_key=j;
                if ( ! lnh_ins_async(1,LNH_key,LNH_key) ) {
                    err++;
                }
            }
            for (j=50000; j<150000;j++) {
                LNH_key=j;
                lnh_ins_async(2,LNH_key,LNH_key);
            }
            lnh_or_sync(1,2,3);
            num=lnh_get_num(3);
            if (num!=150000) {
                err++;
            }
            for (j=0;j<150000;j++) {
                LNH_key=j;
                if ( ! lnh_search(3,LNH_key) ) {
                    err++;
                }
            }

            lnh_del_str_sync(1);
            lnh_del_str_sync(2);
            lnh_del_str_sync(3);

        }

    }
    
    mq_send(err);
    
}

void exp8 () {

    //-------------------------------------------------------------
    // тест and на random данных
    //-------------------------------------------------------------

    {
        
        err=0;

        for (i = 0; i<ITERATIONS_COUNT; i++) {

            for (low=0;low<10;low++) {

                num=num1=0;

                for (j=0; j<8*3072;j++) {
                    LNH_key=rand_single()%1000;
                    if ( ! lnh_ins_async(2,LNH_key,LNH_key) ) {
                        err++;
                    }
                }
                lnh_sq_async(2);
                for (j=0; j<8*3072;j++) {
                    LNH_key=rand_single()%1000;
                    if ( ! lnh_ins_async(3,LNH_key,LNH_key) ) {
                        err++;
                    }
                }

                lnh_sq_async(3);

                lnh_and_sync(2,3,4);
                num=lnh_get_num(4);
                num1=0;

                while (lnh_get_first(2)) {
                    //check it is stored in str#3
                    LNH_key = lnh_core.result.key;
                    if ( lnh_search(3,LNH_key) ) {
                        if (! lnh_search(4,LNH_key) ) {
                            err++;
                        };
                        num1++;
                        lnh_del_sync(4,LNH_key);
                    }
                    lnh_del_async(2,LNH_key);
                    lnh_del_async(3,LNH_key);
                }

                if (lnh_get_first(4)) {
                    err++;
                };

                if (num1!=num){
                    err++;
                }

                if (lnh_get_num(4)!=0) {
                    err++;
                }
                lnh_del_str_sync(2);
                lnh_del_str_sync(3);
                lnh_del_str_sync(4);
            }
        }
    }

    mq_send(err);
    
}

void exp9 () {
    
    //-------------------------------------------------------------
    // тест not
    //-------------------------------------------------------------

    {
        
        err=0;

        for (i = 0; i<ITERATIONS_COUNT; i++) {

            for (low=0;low<10;low++) {

                num=num1=0;

                for (j=0; j<8*3072;j++) {
                    LNH_key=rand_single()%1000;
                    if ( ! lnh_ins_async(2,LNH_key,LNH_key) ) {
                        err++;
                    }
                }
                lnh_sq_async(2);
                for (j=0; j<8*3072;j++) {
                    LNH_key=rand()%1000;
                    if ( ! lnh_ins_async(3,LNH_key,LNH_key) ) {
                        err++;
                    }
                }
                lnh_sq_sync(3);
                num=lnh_get_num(4);
                num1=0;
                while (lnh_get_first(2)) {
                    //check it is stored in str#3
                    LNH_key = lnh_core.result.key;
                    if (! lnh_search(3,LNH_key) ) {
                        if (! lnh_search(4,LNH_key) ) {
                            err++;
                        };
                        num1++;
                        lnh_del_sync(4,LNH_key);
                    }
                    lnh_del_sync(2,LNH_key);
                    lnh_del_sync(3,LNH_key);
                }
                if (lnh_get_first(4)) {
                    err++;
                };
                if (num1!=num){
                    err++;
                }
                if (lnh_get_num(4)!=0) {
                    err++;
                }
                lnh_del_str_sync(2);
                lnh_del_str_sync(3);
                lnh_del_str_sync(4);
            }
        }
    }

    mq_send(err);

}

void exp10 () {

    //-------------------------------------------------------------
    // тест or на random данных
    //-------------------------------------------------------------

    {
        
        err=0;

        for (i = 0; i<ITERATIONS_COUNT; i++) {
            for (low=0;low<10;low++) {
                num=num1=0;
                for (j=0; j<8*3072;j++) {
                    LNH_key=rand_single()%1000;
                    if ( ! lnh_ins_async(2,LNH_key,LNH_key) ) {
                        err++;
                    }
                }

                lnh_sq_async(2);

                for (j=0; j<8*3072;j++) {
                    LNH_key=rand()%1000;
                    if ( ! lnh_ins_async(3,LNH_key,LNH_key) ) {
                        err++;
                    }
                }
                lnh_sq_async(3);

                if (! lnh_or_sync(2,3,4) ) {
                    err++;
                }
                int num2=lnh_get_num(2);
                int num3=lnh_get_num(3);
                num=lnh_get_num(4);
                num1=0;
                while (lnh_get_first(2)) {
                    //check it is stored in str#3
                    LNH_key = lnh_core.result.key;
                    if (! lnh_search(4,LNH_key) ) {
                        err++;
                    };
                    num1++;
                    lnh_del_sync(2,LNH_key);
                    lnh_del_sync(3,LNH_key);
                    lnh_del_sync(4,LNH_key);
                }

                while (lnh_get_first(3)) {
                    //check it is stored in str#3
                    LNH_key = lnh_core.result.key;
                    if (! lnh_search(4,LNH_key) ) {
                        err++;
                    };
                    num1++;
                    lnh_del_sync(2,LNH_key);
                    lnh_del_sync(3,LNH_key);
                    lnh_del_sync(4,LNH_key);
                }


                if (lnh_get_first(4)) {
                    err++;
                };

                if (num1!=num){
                    err++;
                }

                if (lnh_get_num(4)!=0) {
                    err++;
                }
                lnh_del_str_sync(2);
                lnh_del_str_sync(3);
                lnh_del_str_sync(4);
            }
        }
    }

    
    mq_send(err);

}



