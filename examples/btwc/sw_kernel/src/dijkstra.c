#include "dijkstra.h"

extern lnh lnh_core;
extern global_memory_io gmio;

#define DEBUG

//-------------------------------------------------------------
// Удаление графа
//-------------------------------------------------------------

void delete_graph() {
    lnh_del_str_async(G);
}

//-------------------------------------------------------------
// Удаление визуализации графа
//-------------------------------------------------------------

void delete_visualization() {
    lnh_del_str_async(vG);
}


//-----------------------------------------------------------------------
// Определение цвета в формате RGB (24 бит) по температуре (1000..12000K)
//-----------------------------------------------------------------------

unsigned int kelvin2rgb(unsigned int temperature) {

    //Temperature must fall between 1000 and 40000 degrees
    if (temperature < 1000) return kelvin_to_rgb[0];
    if (temperature > 12000) return kelvin_to_rgb[110];
    return kelvin_to_rgb[(temperature-1000)/100];
    
}


//-----------------------------------------------------------------------
// Определение цвета в формате RGB (24 бит) по интенсивности (0..255)
//-----------------------------------------------------------------------

unsigned int violet2rgb(unsigned int uv) {

    if (uv > 255) uv = 255;
    return (uv<<16)|((uv>>2)<<8)|((0xFF-uv)<<0);
}


//-------------------------------------------------------------
// Создание визуализации графа в виде решетки 20x20
//-------------------------------------------------------------

void create_visualization() {
 
    //Удаление структуры визуализации
    lnh_del_str_async(vG);
    //Копирование графа G -> vG
    lnh_greq_sync(0ull,G,vG);
    //Переменные
    unsigned int u;
    unsigned short int x;
    unsigned short int y;
    unsigned short int size;
    unsigned int color;
    unsigned int btwc,btwc_max = 0;
    //Обход всех вершин графа и определение максимального значения btwc
    lnh_get_first(vG);
    do {
        //Перебор индексов вершин u
        u = (*(u_key*)&lnh_core.result.key).__struct.u;
        lnh_search(vG,INLINE(u_key,{.index=PTH_IDX,.u=u}));
        btwc = (*(u_index*)&lnh_core.result.value).__struct.btwc;
        if (btwc>btwc_max) btwc_max = btwc;
    } while (lnh_ngr(vG,INLINE(u_key,{.index=IDX_MAX,.u=u}))); //переход к следующей вершине


    lnh_get_first(vG);
    do {
        //Перебор индексов вершин u
        u = (*(u_key*)&lnh_core.result.key).__struct.u;
        x=X_MAX*(u%20)/20;//rand_single()%X_MAX;
        y=Y_MAX*(u/20)/20;//rand_single()%Y_MAX;
        lnh_search(vG,INLINE(u_key,{.index=PTH_IDX,.u=u}));
        btwc = (*(u_index*)&lnh_core.result.value).__struct.btwc;
        size=(S_MAX*btwc)/btwc_max+10;
        unsigned int intensity = (0xFF*btwc)/btwc_max;
        color=violet2rgb(intensity); //Light color
        //color=kelvin2rgb(intensity);
        lnh_ins_async(vG,INLINE(u_key,{.index=VATR_IDX,.u=u}),INLINE(visualization_attributes,{.x=x,.y=y,.size=size,.color=color}));    
    } while (lnh_ngr(vG,INLINE(u_key,{.index=IDX_MAX,.u=u}))); //переход к следующей вершине



}


//------------------------------------------------------------------------------
// Создание визуализации графа на основе обхода матрицы 
//------------------------------------------------------------------------------

void create_centrality_visualization() {
 
    //Удаление структуры визуализации
    lnh_del_str_async(vG);
    //Копирование графа G -> vG
    lnh_greq_sync(0ull,G,vG);
    //Переменные
    unsigned int u;
    unsigned int color;
    unsigned int btwc,btwc_max = 0;
    unsigned int virtex_count = 0;
    enum { down,left,up,right } direction;
    short unsigned int max_size;
    short unsigned int x_mid = (X_MAX>>1);
    short unsigned int y_mid = (Y_MAX>>1);
    short unsigned int x;
    short unsigned int y;
    short unsigned int size;
    unsigned int state = down;//down
    const short int space = S_MAX/2;


    //Очистка очереди
    lnh_del_str_async(Q);
    //Обход всех вершин графа и определение максимального значения btwc и количества вершин
    lnh_get_first(vG);
    do {
        //Перебор индексов вершин u
        u = (*(u_key*)&lnh_core.result.key).__struct.u;
        lnh_search(vG,INLINE(u_key,{.index=PTH_IDX,.u=u}));
        btwc = (*(u_index*)&lnh_core.result.value).__struct.btwc;
        //Запись очереди вершин
        lnh_ins_async(Q,INLINE(q_record,{.u=u,.index=btwc}),0);
    } while (lnh_ngr(vG,INLINE(u_key,{.index=IDX_MAX,.u=u}))); //переход к следующей вершине


    virtex_count = lnh_get_num(Q);    
    lnh_get_last(Q);
    btwc_max = (*(q_record*)&lnh_core.result.key).__struct.index;
    //set starting position
    x = x_mid+S_MAX+10;
    y = y_mid;
    max_size = S_MAX+10;

    short unsigned int left_up_x = x_mid - S_MAX - 10;
    short unsigned int left_up_y = y_mid - S_MAX - 10;
    short unsigned int right_down_x = x_mid + S_MAX + 10;
    short unsigned int right_down_y = y_mid + S_MAX + 10;
 
    do {
        //Перебор индексов вершин u
        u = (*(q_record*)&lnh_core.result.key).__struct.u;
        //Полечение центральности
        btwc = (*(q_record*)&lnh_core.result.key).__struct.index;
        //Размер вершины
        size=(S_MAX*btwc)/btwc_max + 10;
        //Цвет вершины
        unsigned int intensity = (0xFF*btwc)/btwc_max;
        color=violet2rgb(intensity); //Light color
        //color=kelvin2rgb(intensity);
        lnh_ins_async(vG,INLINE(u_key,{.index=VATR_IDX,.u=u}),INLINE(visualization_attributes,{.x=x,.y=y,.size=size,.color=color}));    
        //Определение положения вершины в пиксельной матрице
        switch (state) {
            case down:
                y += max_size;
                if (y>=right_down_y) {
                    state = left;
                    x = right_down_x - max_size;
                    y = right_down_y;
                }
                break;
            case left:
                x -= max_size;
                if (x<=left_up_x) {
                    state = up;
                    x = left_up_x;
                    y = right_down_y - max_size;
                }
                break;
            case up: //up
                y -= max_size;
                if (y<=left_up_y) {
                    state = right;
                    x = left_up_x + max_size;
                    y = left_up_y;
                }
                break;
            case right: //right
                x += max_size;
                if (x>=right_down_x) {
                    state = down;
                    y = right_down_y - max_size;
                    max_size = size + space;
                    left_up_x = left_up_x - max_size;
                    left_up_y = left_up_y - max_size;
                    right_down_x = right_down_x + max_size;
                    right_down_y = right_down_y + max_size;
                    x = right_down_x;
                    y = left_up_y + max_size;
                }
                break;
        }
    } while (lnh_nsm(Q,INLINE(q_record,{.u=u,.index=btwc}))); //переход к предыдущей вершине



}

//------------------------------------------------------------------------------
// Создание визуализации графа на основе обхода по спирали центральности
//------------------------------------------------------------------------------

void create_centrality_spiral_visualization() {
 
    //Удаление структуры визуализации
    lnh_del_str_async(vG);
    //Копирование графа G -> vG
    lnh_greq_sync(0ull,G,vG);
    //Переменные
    unsigned int u;
    unsigned int color;
    unsigned int btwc,btwc_max = 0;
    unsigned int virtex_count = 0;
    short unsigned int x_mid = (X_MAX>>1);
    short unsigned int y_mid = (Y_MAX>>1);
    short unsigned int x;
    short unsigned int y;
    short unsigned int size;
    short unsigned int max_size = S_MAX + 10;

    const short int space = S_MAX/2;

    //Очистка очереди
    lnh_del_str_async(Q);
    //Обход всех вершин графа и определение максимального значения btwc и количества вершин
    lnh_get_first(vG);
    do {
        //Перебор индексов вершин u
        u = (*(u_key*)&lnh_core.result.key).__struct.u;
        lnh_search(vG,INLINE(u_key,{.index=PTH_IDX,.u=u}));
        btwc = (*(u_index*)&lnh_core.result.value).__struct.btwc;
        //Запись очереди вершин
        lnh_ins_async(Q,INLINE(q_record,{.u=u,.index=btwc}),0);
    } while (lnh_ngr(vG,INLINE(u_key,{.index=IDX_MAX,.u=u}))); //переход к следующей вершине


    virtex_count = lnh_get_num(Q);    
    lnh_get_last(Q);
    btwc_max = (*(q_record*)&lnh_core.result.key).__struct.index;
    //set starting position
    float angle = 2 * M_PI;
    int virtex_radius = S_MAX+50;
    int radius;

    do {
        //Перебор индексов вершин u
        u = (*(q_record*)&lnh_core.result.key).__struct.u;
        //Полечение центральности
        btwc = (*(q_record*)&lnh_core.result.key).__struct.index;
        //Размер вершины
        size=(S_MAX*btwc)/btwc_max + 10;
        //Цвет вершины
        unsigned int intensity = (0xFF*btwc)/btwc_max;
        color=violet2rgb(intensity); //Light color
        //color=kelvin2rgb(intensity);
        radius = float(virtex_radius) * angle / (2 * M_PI) ;
        x = x_mid + radius * cos(angle);
        y = y_mid + radius * sin(angle);
        lnh_ins_async(vG,INLINE(u_key,{.index=VATR_IDX,.u=u}),INLINE(visualization_attributes,{.x=x,.y=y,.size=size,.color=color}));    
        //Определение положения вершины в пиксельной матрице
        angle += acos(1.0 - float(pow(S_MAX,2))/float(2*pow(radius,2))); 
    } while (lnh_nsm(Q,INLINE(q_record,{.u=u,.index=btwc}))); //переход к предыдущей вершине



}


//------------------------------------------------------------------------------
// Рекурсивная функция для определения границ визуализации сообществ
//------------------------------------------------------------------------------

void set_xy(unsigned int comunity, unsigned int x0, unsigned int y0, unsigned int x1, unsigned int y1, bool horisontal) {

    unsigned int left_leaf,right_leaf;
    unsigned int left_v_count,right_v_count;
    unsigned int left_x0,left_y0,left_x1,left_y1;
    unsigned int right_x0,right_y0,right_x1,right_y1;
    bool is_leaf;
    unsigned int left_factor;


    //Получить информацию о кратности сообщества
    lnh_search(cvG,INLINE(сom_viz_key,{.adj=CMTY_VCOUNT_IDX,.comunity=comunity}));
    is_leaf = (*(сom_viz_vcount*)&lnh_core.result.value).__struct.is_leaf;

    //Отладочное сообщение
    #ifdef DEBUG
    mq_send(-5);
    mq_send(comunity);
    //Получить информацию о сообществе

    mq_send((*(сom_viz_vcount*)&lnh_core.result.value).__struct.v_count);         
    mq_send(x0);
    mq_send(y0);
    mq_send(x1);
    mq_send(y1);
    mq_send(is_leaf);
    #endif

    if (is_leaf) {
        lnh_ins_async(cvG,INLINE(сom_viz_key,{.adj=CMTY_XY_IDX,.comunity=comunity}),INLINE(сom_viz_xy,{.x0=(short unsigned int)x0,.y0=(short unsigned int)y0,.x1=(short unsigned int)x1,.y1=(short unsigned int)y1}));
    } else {
        //Получить информацию о левом и правом сообществах-потомках
        lnh_nsm(cvG,INLINE(сom_viz_key,{.adj=CMTY_XY_IDX,.comunity=comunity}));
        left_leaf=(*(сom_viz_index*)&lnh_core.result.value).__struct.left_leaf;            
        right_leaf=(*(сom_viz_index*)&lnh_core.result.value).__struct.right_leaf;   
        //Запись о сообществе можно удалять
        lnh_del_async(cvG,INLINE(сom_viz_key,{.adj=(*(сom_viz_key*)&lnh_core.result.key).__struct.adj,.comunity=comunity}));       
        lnh_del_async(cvG,INLINE(сom_viz_key,{.adj=CMTY_VCOUNT_IDX,.comunity=comunity}));       
        lnh_del_async(cvG,INLINE(сom_viz_key,{.adj=CMTY_XY_IDX,.comunity=comunity}));       
        //Получить информацию о левом сообществе
        lnh_search(cvG,INLINE(сom_viz_key,{.adj=CMTY_VCOUNT_IDX,.comunity=left_leaf}));
        left_v_count=(*(сom_viz_vcount*)&lnh_core.result.value).__struct.v_count;         
        //Получить информацию о правом сообществе
        lnh_search(cvG,INLINE(сom_viz_key,{.adj=CMTY_VCOUNT_IDX,.comunity=right_leaf}));
        right_v_count=(*(сom_viz_vcount*)&lnh_core.result.value).__struct.v_count;  
        left_factor = (100*left_v_count)/(left_v_count+right_v_count);
        if (horisontal) {
            //Left
            left_x0=x0;
            left_y0=y0;
            left_x1=x0+(left_factor*(x1-x0))/100;
            left_y1=y1;
            set_xy(left_leaf,left_x0,left_y0,left_x1,left_y1,false);
            //Right
            right_x0=x0+(left_factor*(x1-x0))/100;
            right_y0=y0;
            right_x1=x1;
            right_y1=y1;
            set_xy(right_leaf,right_x0,right_y0,right_x1,right_y1,false);
        } else {
            //Left
            left_x0=x0;
            left_y0=y0;
            left_x1=x1;
            left_y1=y0+(left_factor*(y1-y0))/100;
            set_xy(left_leaf,left_x0,left_y0,left_x1,left_y1,true);
            //Right
            right_x0=x0;
            right_y0=y0+(left_factor*(y1-y0))/100;
            right_x1=x1;
            right_y1=y1;            
            set_xy(right_leaf,right_x0,right_y0,right_x1,right_y1,true);
        }
    }
}


//------------------------------------------------------------------------------
// Выделение сильно связанных сообществ на основе модулярности
//------------------------------------------------------------------------------

void create_communities_forest_vizualization() {

    short int adj_c,adj_ic;
    short int wu;
    bool flag;
    unsigned int adj_u,adj_v,adj_k;
    unsigned int w, w_sum = 0;
    unsigned int first_virtex_u,first_virtex_v,last_virtex_u,last_virtex_v;
    unsigned int m = 0;
    unsigned int n = 0;
    int delta_mod = 0;
    int com_u_delta_mod, com_v_delta_mod, com_k_delta_mod;
    unsigned int mq_index = 0;
    unsigned int com_u_index,com_v_index,com_k_index;
    unsigned int com_u,com_v,com_k,com_r;
    unsigned int w_u_v,adj_u_v,w_k_v,w_k_u,adj_k_v;
    unsigned int com_u_virtex,com_v_virtex;
    int modularity=0;
    unsigned int v_count;
    unsigned int btwc,btwc_max=0;


    /************************************************************************************
     * 
        I - Инициализация структур
     *
    *************************************************************************************/

 
    //Удаление структуры сообществ
    lnh_del_str_async(cG);
    
    //удаление очереди модулярности
    lnh_del_str_async(mQ);
    
    //удаление структуры индексов
    lnh_del_str_async(iQ);

    //Обход всех вершин графа для определения количества полуребер (2*m) и количества вершин (n)
    lnh_get_first(G);
    do {

        //Перебор индексов вершин u
        com_u = (*(u_key*)&lnh_core.result.key).__struct.u;
        
        //Инициировать цепь записей о вершинах в сообществе в поле pu.
        lnh_search(G,INLINE(u_key,{.index=BASE_IDX,.u=com_u}));
        adj_c = (*(u_attributes*)&lnh_core.result.value).__struct.adj_c;
        lnh_search(G,INLINE(u_key,{.index=PTH_IDX,.u=com_u}));
        btwc = (*(u_index*)&lnh_core.result.value).__struct.btwc;
	if (btwc_max<btwc) btwc_max=btwc;

        //Для каждого ребра создать запись в очередь модулярности и в структуру индексов
        for (unsigned int i=0;i<adj_c;i++) {
     
            //Получить информацию о ребре Adj[i]
            lnh_search(G,INLINE(u_key,{.index=i,.u=com_u}));

            //Вычислить сумму весов ребер
            w_sum += (*(edge*)&lnh_core.result.value).__struct.w;
        
        }

        //Добавить кратность вершины к m
        m+=adj_c;
        n++;

    } while (lnh_ngr(G,INLINE(u_key,{.index=IDX_MAX,.u=com_u}))); //переход к следующей вершине

    //Средний вес ребра = сумма весов всех полуребер / количество полуребер
    w = w_sum / m;

    //Обход всех вершин графа, создание очереди модулярности, структуры индексов и структуры сообществ, вычисление среднего веса ребра
    lnh_get_first(G);

    do {

        //Перебор индексов сообществ
        com_u = (*(u_key*)&lnh_core.result.key).__struct.u;
        lnh_search(G,INLINE(u_key,{.index=BASE_IDX,.u=com_u}));
        adj_c = (*(u_attributes*)&lnh_core.result.value).__struct.adj_c;

        //В начале алгоритма номер вершины совпадает с начальным сообществом. Записываем в поле pu для создания цепочки вершин.
        lnh_ins_async(G,INLINE(u_key,{.index=BASE_IDX,.u=com_u}),INLINE(u_attributes,{.pu=com_u, .eQ=false, .non=0, .adj_c=adj_c}));

        //Создать запись о сообщесте, начальная и конечные вершины указывают на вершину с аналогичным номером
        lnh_ins_async(cG,INLINE(сom_u_key,{.adj=(unsigned int)adj_c,.comunity=com_u}),INLINE(сom_u_index,{.first_virtex=com_u, .last_virtex=com_u}));

        //Для каждого ребра создать запись в очередь модулярности и в структуру индексов
        for (unsigned int i=0;i<adj_c;i++) {

            //Получить информацию о ребре Adj[i]
            lnh_search(G,INLINE(u_key,{.index=i,.u=com_u}));
            wu = (*(edge*)&lnh_core.result.value).__struct.w;
            com_v = (*(edge*)&lnh_core.result.value).__struct.v;

            //Определить степень инцидентной вершины
            lnh_search(G,INLINE(u_key,{.index=BASE_IDX,.u=com_v}));
            adj_ic = (*(u_attributes*)&lnh_core.result.value).__struct.adj_c;

            //Вычислить целочисленную модулярность вершин при объединении
            delta_mod = m*wu-w*adj_c*adj_ic; 

            //Определить номер в очереди за последним индексом для сегмента очереди delta_mod
            if ( lnh_nsm(mQ,INLINE(q_modularity,{.index=0,.delta_mod=delta_mod+1})) && ((*(q_modularity*)&lnh_core.result.key).__struct.delta_mod == delta_mod) )
                mq_index = (*(q_modularity*)&lnh_core.result.key).__struct.index + 2; //Одна запись сообщества занимает два индекса
            else
                mq_index = 1;

            //Добавить запись об атрибутах в очередь
            lnh_ins_async(mQ,INLINE(q_modularity,{.index=mq_index-1,.delta_mod=delta_mod}),INLINE(q_attributes,{.w_u_v=(unsigned int)wu,.non=0}));

            //Добавить запись о модулярности в следующий индекс
            lnh_ins_async(mQ,INLINE(q_modularity,{.index=mq_index,.delta_mod=delta_mod}),INLINE(q_comminities,{.com_u=com_v,.com_v=com_u}));

            //Добавить запись в структуру индексов
            lnh_ins_async(iQ,INLINE(q_comminities,{.com_u=com_v,.com_v=com_u}),INLINE(q_modularity,{.index=mq_index,.delta_mod=delta_mod}));

        }
    } while (lnh_ngr(G,INLINE(u_key,{.index=IDX_MAX,.u=com_u}))); //переход к следующей вершине

    //Отладочное сообщение
    #ifdef DEBUG
    lnh_sync();
    mq_send(-1);
    unsigned int mq_=lnh_get_num(mQ);
    unsigned int cg_=lnh_get_num(cG);
    mq_send(mq_);
    mq_send(cg_);
    mq_send(n);
    #endif

    //Отладочное сообщение
    #ifdef DEBUG
    mq_send(0);
    #endif


    /************************************************************************************
     * 
        II - Выделение сообществ до максимального значения модулярности
     *
    *************************************************************************************/

    //Основной цикл
    while( (lnh_get_num(mQ)>1) && lnh_get_last_signed(mQ)  && ( (*(q_modularity*)&lnh_core.result.key).__struct.delta_mod > 0) ) {
        
        //Получить номера объединяемых сообществ
        com_u = (*(q_comminities*)&lnh_core.result.value).__struct.com_v;
        com_v = (*(q_comminities*)&lnh_core.result.value).__struct.com_u;
        com_u_delta_mod = (*(q_modularity*)&lnh_core.result.key).__struct.delta_mod;
        com_u_index = (*(q_modularity*)&lnh_core.result.key).__struct.index;

        //Получить информацию об атрибутах связности u и v
        lnh_search(mQ,INLINE(q_modularity,{.index=com_u_index-1,.delta_mod=com_u_delta_mod}));
        w_u_v=(*(q_attributes*)&lnh_core.result.value).__struct.w_u_v;

         //Найти запись о связи сообществ v и u в структуре индексов
        lnh_search(iQ,INLINE(q_comminities,{.com_u=com_u,.com_v=com_v})); 
        com_v_delta_mod = (*(q_modularity*)&lnh_core.result.value).__struct.delta_mod;
        com_v_index = (*(q_modularity*)&lnh_core.result.value).__struct.index;

        //Удалить запись о модулярности связи сообществ u и v
        lnh_del_async(mQ,INLINE(q_modularity,{.index=com_u_index,.delta_mod=com_u_delta_mod})); 

        //Удалить запись с атрибутами
        lnh_del_async(mQ,INLINE(q_modularity,{.index=com_u_index-1,.delta_mod=com_u_delta_mod})); 

        //Удалить запись о связи сообществ u и v из структуры индексов
        lnh_del_async(iQ,INLINE(q_comminities,{.com_u=com_v,.com_v=com_u})); 


        //Удалить запись о модулярности связи сообществ v и u
        lnh_del_async(mQ,INLINE(q_modularity,{.index=com_v_index,.delta_mod=com_v_delta_mod})); 

        //Удалить запись с атрибутами
        lnh_del_async(mQ,INLINE(q_modularity,{.index=com_v_index-1,.delta_mod=com_v_delta_mod})); 

        //Удалить запись о связи сообществ v и u из структуры индексов
        lnh_del_async(iQ,INLINE(q_comminities,{.com_u=com_u,.com_v=com_v})); 

        //Обновить модуляность
        modularity+=com_u_delta_mod;
        
        //Отладочное сообщение
        #ifdef DEBUG
        mq_send(-1);
        mq_send(com_u);
        mq_send(com_v);
        mq_send(com_u_delta_mod);
        mq_send(modularity);
        #endif
        
        //Получить информацию о вершинах сообщества v
        lnh_nsm(cG,INLINE(сom_u_key,{.adj=0,.comunity=com_v+1}));
        adj_v=(*(сom_u_key*)&lnh_core.result.key).__struct.adj;
        first_virtex_v=(*(сom_u_index*)&lnh_core.result.value).__struct.first_virtex;
        last_virtex_v=(*(сom_u_index*)&lnh_core.result.value).__struct.last_virtex;

        //Удалить сообщество v и структуры сообществ
        lnh_del_async(cG,INLINE(сom_u_key,{.adj=adj_v,.comunity=com_v}));
        
        //Получить информацию о вершинах сообщества u
        lnh_nsm(cG,INLINE(сom_u_key,{.adj=0,.comunity=com_u+1}));
        adj_u=(*(сom_u_key*)&lnh_core.result.key).__struct.adj;
        first_virtex_u=(*(сom_u_index*)&lnh_core.result.value).__struct.first_virtex;
        last_virtex_u=(*(сom_u_index*)&lnh_core.result.value).__struct.last_virtex;

        //Удалить сообщество u из структуры сообществ
        lnh_del_async(cG,INLINE(сom_u_key,{.adj=adj_u,.comunity=com_u}));
        
        //Объединить цепочки записей сообществ в графе
        lnh_search(G,INLINE(u_key,{.index=BASE_IDX,.u=last_virtex_u}));
        lnh_ins_async(G,INLINE(u_key,{.index=BASE_IDX,.u=last_virtex_u}),INLINE(u_attributes,{.pu=first_virtex_v, .eQ=false, .non=0, .adj_c=(*(u_attributes*)&lnh_core.result.value).__struct.adj_c}));

        //Обновить информацию о конечной вершине цепочки для сообщества u
        lnh_ins_async(cG,INLINE(сom_u_key,{.adj=adj_u+adj_v-2,.comunity=com_u}),INLINE(сom_u_index,{.first_virtex=first_virtex_u, .last_virtex=last_virtex_v}));


        //Обойти все записи о сообществе u в очереди модулярности
        flag = lnh_nsm(iQ,INLINE(q_comminities,{.com_u=0,.com_v=com_u+1}));

        //Перебор структуры индексов смежных сообществ с сообществом com_v
        while (flag && ((*(q_comminities*)&lnh_core.result.key).__struct.com_v == com_u)) {

            //Получить индекс и модулярность из структуры инексов
            com_k = (*(q_comminities*)&lnh_core.result.key).__struct.com_u;
            com_k_index=(*(q_modularity*)&lnh_core.result.value).__struct.index;
            com_k_delta_mod=(*(q_modularity*)&lnh_core.result.value).__struct.delta_mod;

            //Отладочное сообщение
            #ifdef DEBUG
            mq_send(-2);
            mq_send(com_k);
            mq_send(com_u);
            mq_send(com_k_delta_mod);
            #endif

            //Для всех кандидатов, кроме com_u
            if (com_k!=com_v)  {

                //Получить атрибуты сообщества из очереди модулярности
                lnh_search(mQ,INLINE(q_modularity,{.index=com_k_index-1,.delta_mod=com_k_delta_mod}));
                w_k_v = (*(q_attributes*)&lnh_core.result.value).__struct.w_u_v; 

                //Получить информацию о кратности сообщества k
                lnh_nsm(cG,INLINE(сom_u_key,{.adj=0,.comunity=com_k+1}));
                adj_k=(*(сom_u_key*)&lnh_core.result.key).__struct.adj;

                //Если сообщество com_k связано и с com_v тоже, то обновление уже произведено
                if (lnh_search(iQ,INLINE(q_comminities,{.com_u=com_v,.com_v=com_k}))) {

                    //Обновление будет произведено для v
                    //Do nothing

                } else {

                    //Если сообщество com_k не связано с com_v

                    //Удалить запись о модулярности связи сообществ u и k
                    lnh_del_async(mQ,INLINE(q_modularity,{.index=com_k_index,.delta_mod=com_k_delta_mod})); 

                    //Удалить запись с атрибутами
                    lnh_del_async(mQ,INLINE(q_modularity,{.index=com_k_index-1,.delta_mod=com_k_delta_mod})); 

                    //Сложить изменения модуляности (Aaron Clauset, M.E.J. Newman and Cristopher Moore. Finding community structure in very large networks, 10.c)
                    com_k_delta_mod = com_k_delta_mod - 2 * adj_u*adj_v*w_u_v;

                   //Определить новый номер в очереди за последним индексом для сегмента очереди com_k_delta_mod
                    if ( lnh_nsm(mQ,INLINE(q_modularity,{.index=0,.delta_mod=com_k_delta_mod+1})) && ((*(q_modularity*)&lnh_core.result.key).__struct.delta_mod == com_k_delta_mod) )
                        com_k_index = (*(q_modularity*)&lnh_core.result.key).__struct.index + 2; //Одна запись занимает два индекса
                    else
                        com_k_index = 1;                    

                    //Получить атрибуты связности сообществ k и u
                    lnh_search(iQ,INLINE(q_comminities,{.com_u=com_u,.com_v=com_k}));
                    delta_mod = (*(q_modularity*)&lnh_core.result.value).__struct.delta_mod; 
                    mq_index = (*(q_modularity*)&lnh_core.result.value).__struct.index; //Индекс записи о связи сообществ k и v

                    //Удалить запись о модулярности связи сообществ k и u
                    lnh_del_async(mQ,INLINE(q_modularity,{.index=mq_index,.delta_mod=delta_mod})); 

                    //Удалить запись с атрибутами
                    lnh_del_async(mQ,INLINE(q_modularity,{.index=mq_index-1,.delta_mod=delta_mod})); 

                    //Добавить запись об атрибутах связи сообществ k и u в очередь
                    lnh_ins_async(mQ,INLINE(q_modularity,{.index=com_k_index-1,.delta_mod=com_k_delta_mod}),INLINE(q_attributes,{.w_u_v=w_k_v,.non=0}));

                    //Добавить запись о модулярности  в следующий индекс
                    lnh_ins_async(mQ,INLINE(q_modularity,{.index=com_k_index,.delta_mod=com_k_delta_mod}),INLINE(q_comminities,{.com_u=com_u,.com_v=com_k}));

                    //Обновить запись в структуру индексов
                    lnh_ins_async(iQ,INLINE(q_comminities,{.com_u=com_u,.com_v=com_k}),INLINE(q_modularity,{.index=com_k_index,.delta_mod=com_k_delta_mod}));

                    com_k_index += 2; //Добавить обратную запись                 

                    //Добавить запись об атрибутах связи сообществ u и k в очередь
                    lnh_ins_async(mQ,INLINE(q_modularity,{.index=com_k_index-1,.delta_mod=com_k_delta_mod}),INLINE(q_attributes,{.w_u_v=w_k_v,.non=0}));

                    //Добавить запись о модулярности  в следующий индекс
                    lnh_ins_async(mQ,INLINE(q_modularity,{.index=com_k_index,.delta_mod=com_k_delta_mod}),INLINE(q_comminities,{.com_u=com_k,.com_v=com_u}));

                    //Добавить запись в структуру индексов
                    lnh_ins_async(iQ,INLINE(q_comminities,{.com_u=com_k,.com_v=com_u}),INLINE(q_modularity,{.index=com_k_index,.delta_mod=com_k_delta_mod}));


                }

            }

            //Перейти к следующему сообществу, связанному с com_u (перебор в обратном порядке)
            flag = lnh_nsm(iQ,INLINE(q_comminities,{.com_u=com_k,.com_v=com_u}));
        }

    
        //Обойти все записи о сообществе v в очереди модулярности
        flag = lnh_nsm(iQ,INLINE(q_comminities,{.com_u=0,.com_v=com_v+1}));

        //Перебор структуры индексов смежных сообществ с сообществом com_v
        while (flag && ((*(q_comminities*)&lnh_core.result.key).__struct.com_v == com_v)) {

            //Получить индекс и модулярность из структуры инексов
            com_k = (*(q_comminities*)&lnh_core.result.key).__struct.com_u;
            com_k_index=(*(q_modularity*)&lnh_core.result.value).__struct.index;
            com_k_delta_mod=(*(q_modularity*)&lnh_core.result.value).__struct.delta_mod;

            //Отладочное сообщение
            #ifdef DEBUG
            mq_send(-2);
            mq_send(com_k);
            mq_send(com_v);
            mq_send(com_k_delta_mod);
            #endif

            //Для всех кандидатов, кроме com_u
            if (com_k!=com_u)  {

                //Получить атрибуты сообщества из очереди модулярности
                lnh_search(mQ,INLINE(q_modularity,{.index=com_k_index-1,.delta_mod=com_k_delta_mod}));
                w_k_v = (*(q_attributes*)&lnh_core.result.value).__struct.w_u_v; 

                //Получить информацию о кратности сообщества k
                lnh_nsm(cG,INLINE(сom_u_key,{.adj=0,.comunity=com_k+1}));
                adj_k=(*(сom_u_key*)&lnh_core.result.key).__struct.adj;

                //Если сообщество com_k связано и с com_u тоже
                if (lnh_search(iQ,INLINE(q_comminities,{.com_u=com_u,.com_v=com_k}))) {

                    //Получить атрибуты связности сообществ k и u
                    delta_mod = (*(q_modularity*)&lnh_core.result.value).__struct.delta_mod; 
                    mq_index = (*(q_modularity*)&lnh_core.result.value).__struct.index; //Индекс записи о связи сообществ k и u
                    lnh_search(mQ,INLINE(q_modularity,{.index=mq_index-1,.delta_mod=delta_mod}));
                    w_k_u = (*(q_attributes*)&lnh_core.result.value).__struct.w_u_v; 

                    //Удалить запись о модулярности связи сообществ v и k
                    lnh_del_async(mQ,INLINE(q_modularity,{.index=com_k_index,.delta_mod=com_k_delta_mod})); 

                    //Удалить запись с атрибутами
                    lnh_del_async(mQ,INLINE(q_modularity,{.index=com_k_index-1,.delta_mod=com_k_delta_mod})); 

                    //Удалить запись о связи сообществ v и k из структуры индексов
                    lnh_del_async(iQ,INLINE(q_comminities,{.com_u=com_k,.com_v=com_v})); 

                    //Удалить запись о модулярности связи сообществ k и u
                    lnh_del_async(mQ,INLINE(q_modularity,{.index=mq_index,.delta_mod=delta_mod})); 

                    //Удалить запись с атрибутами
                    lnh_del_async(mQ,INLINE(q_modularity,{.index=mq_index-1,.delta_mod=delta_mod})); 

                    //Сложить изменения модуляности (Aaron Clauset, M.E.J. Newman and Cristopher Moore. Finding community structure in very large networks, 10.a)
                    com_k_delta_mod = com_k_delta_mod + delta_mod;

                    //Получить атрибуты связности сообществ u и k
                    lnh_search(iQ,INLINE(q_comminities,{.com_u=com_k,.com_v=com_u}));
                    delta_mod = (*(q_modularity*)&lnh_core.result.value).__struct.delta_mod; 
                    mq_index = (*(q_modularity*)&lnh_core.result.value).__struct.index; //Индекс записи о связи сообществ u и k

                    //Удалить запись о модулярности связи сообществ u и k
                    lnh_del_async(mQ,INLINE(q_modularity,{.index=mq_index,.delta_mod=delta_mod})); 

                    //Удалить запись с атрибутами
                    lnh_del_async(mQ,INLINE(q_modularity,{.index=mq_index-1,.delta_mod=delta_mod}));                                        

                   //Определить новый номер в очереди за последним индексом для сегмента очереди com_k_delta_mod
                    if ( lnh_nsm(mQ,INLINE(q_modularity,{.index=0,.delta_mod=com_k_delta_mod+1})) && ((*(q_modularity*)&lnh_core.result.key).__struct.delta_mod == com_k_delta_mod) )
                        com_k_index = (*(q_modularity*)&lnh_core.result.key).__struct.index + 2; //Одна запись занимает два индекса
                    else
                        com_k_index = 1;                    

                    //Получить атрибуты связности сообществ k и v
                    lnh_search(iQ,INLINE(q_comminities,{.com_u=com_v,.com_v=com_k}));
                    delta_mod = (*(q_modularity*)&lnh_core.result.value).__struct.delta_mod; 
                    mq_index = (*(q_modularity*)&lnh_core.result.value).__struct.index; //Индекс записи о связи сообществ k и v

                    //Удалить запись о модулярности связи сообществ k и v
                    lnh_del_async(mQ,INLINE(q_modularity,{.index=mq_index,.delta_mod=delta_mod})); 

                    //Удалить запись с атрибутами
                    lnh_del_async(mQ,INLINE(q_modularity,{.index=mq_index-1,.delta_mod=delta_mod})); 

                    //Удалить запись о связи сообществ v и k из структуры индексов
                    lnh_del_async(iQ,INLINE(q_comminities,{.com_u=com_v,.com_v=com_k}));                 

                    //Добавить запись об атрибутах связи сообществ k и u в очередь
                    lnh_ins_async(mQ,INLINE(q_modularity,{.index=com_k_index-1,.delta_mod=com_k_delta_mod}),INLINE(q_attributes,{.w_u_v=w_k_u+w_k_v,.non=0}));

                    //Добавить запись о модулярности  в следующий индекс
                    lnh_ins_async(mQ,INLINE(q_modularity,{.index=com_k_index,.delta_mod=com_k_delta_mod}),INLINE(q_comminities,{.com_u=com_u,.com_v=com_k}));

                    //Изменить запись в структуру индексов (запись существует)
                    lnh_ins_async(iQ,INLINE(q_comminities,{.com_u=com_u,.com_v=com_k}),INLINE(q_modularity,{.index=com_k_index,.delta_mod=com_k_delta_mod}));

                    com_k_index += 2; //Добавить обратную запись                 

                    //Добавить запись об атрибутах связи сообществ u и k в очередь
                    lnh_ins_async(mQ,INLINE(q_modularity,{.index=com_k_index-1,.delta_mod=com_k_delta_mod}),INLINE(q_attributes,{.w_u_v=w_k_u+w_k_v,.non=0}));

                    //Добавить запись о модулярности  в следующий индекс
                    lnh_ins_async(mQ,INLINE(q_modularity,{.index=com_k_index,.delta_mod=com_k_delta_mod}),INLINE(q_comminities,{.com_u=com_k,.com_v=com_u}));

                    //Добавить запись в структуру индексов
                    lnh_ins_async(iQ,INLINE(q_comminities,{.com_u=com_k,.com_v=com_u}),INLINE(q_modularity,{.index=com_k_index,.delta_mod=com_k_delta_mod}));

                } else {

                    //Если сообщество com_k не связано с com_u

                    //Удалить запись о модулярности связи сообществ v и k
                    lnh_del_async(mQ,INLINE(q_modularity,{.index=com_k_index,.delta_mod=com_k_delta_mod})); 

                    //Удалить запись с атрибутами
                    lnh_del_async(mQ,INLINE(q_modularity,{.index=com_k_index-1,.delta_mod=com_k_delta_mod})); 

                    //Удалить запись о связи сообществ v и k из структуры индексов
                    lnh_del_async(iQ,INLINE(q_comminities,{.com_u=com_k,.com_v=com_v})); 


                    //Сложить изменения модуляности (Aaron Clauset, M.E.J. Newman and Cristopher Moore. Finding community structure in very large networks, 10.b)
                    com_k_delta_mod = com_k_delta_mod - 2 * adj_u*adj_v*w_u_v;


                   //Определить новый номер в очереди за последним индексом для сегмента очереди com_k_delta_mod
                    if ( lnh_nsm(mQ,INLINE(q_modularity,{.index=0,.delta_mod=com_k_delta_mod+1})) && ((*(q_modularity*)&lnh_core.result.key).__struct.delta_mod == com_k_delta_mod) )
                        com_k_index = (*(q_modularity*)&lnh_core.result.key).__struct.index + 2; //Одна запись занимает два индекса
                    else
                        com_k_index = 1;                    

                    //Получить атрибуты связности сообществ k и v
                    lnh_search(iQ,INLINE(q_comminities,{.com_u=com_v,.com_v=com_k}));
                    delta_mod = (*(q_modularity*)&lnh_core.result.value).__struct.delta_mod; 
                    mq_index = (*(q_modularity*)&lnh_core.result.value).__struct.index; //Индекс записи о связи сообществ k и v

                    //Удалить запись о модулярности связи сообществ k и v
                    lnh_del_async(mQ,INLINE(q_modularity,{.index=mq_index,.delta_mod=delta_mod})); 

                    //Удалить запись с атрибутами
                    lnh_del_async(mQ,INLINE(q_modularity,{.index=mq_index-1,.delta_mod=delta_mod})); 

                    //Удалить запись о связи сообществ v и k из структуры индексов
                    lnh_del_async(iQ,INLINE(q_comminities,{.com_u=com_v,.com_v=com_k}));                 

                    //Добавить запись об атрибутах связи сообществ k и u в очередь
                    lnh_ins_async(mQ,INLINE(q_modularity,{.index=com_k_index-1,.delta_mod=com_k_delta_mod}),INLINE(q_attributes,{.w_u_v=w_k_v,.non=0}));

                    //Добавить запись о модулярности  в следующий индекс
                    lnh_ins_async(mQ,INLINE(q_modularity,{.index=com_k_index,.delta_mod=com_k_delta_mod}),INLINE(q_comminities,{.com_u=com_u,.com_v=com_k}));

                    //Добавить запись в структуру индексов
                    lnh_ins_async(iQ,INLINE(q_comminities,{.com_u=com_u,.com_v=com_k}),INLINE(q_modularity,{.index=com_k_index,.delta_mod=com_k_delta_mod}));

                    com_k_index += 2; //Добавить обратную запись                 

                    //Добавить запись об атрибутах связи сообществ u и k в очередь
                    lnh_ins_async(mQ,INLINE(q_modularity,{.index=com_k_index-1,.delta_mod=com_k_delta_mod}),INLINE(q_attributes,{.w_u_v=w_k_v,.non=0}));

                    //Добавить запись о модулярности  в следующий индекс
                    lnh_ins_async(mQ,INLINE(q_modularity,{.index=com_k_index,.delta_mod=com_k_delta_mod}),INLINE(q_comminities,{.com_u=com_k,.com_v=com_u}));

                    //Добавить запись в структуру индексов
                    lnh_ins_async(iQ,INLINE(q_comminities,{.com_u=com_k,.com_v=com_u}),INLINE(q_modularity,{.index=com_k_index,.delta_mod=com_k_delta_mod}));


                }

            }

            //Перейти к следующему сообществу, связанному с com_v (перебор в обратном порядке)
            flag = lnh_nsm(iQ,INLINE(q_comminities,{.com_u=com_k,.com_v=com_v}));
        }

    }

    //Отладочное сообщение
    #ifdef DEBUG
    mq_send(0);
    #endif

    /************************************************************************************
     * 
        III - Создание дерева сообществ для формирования блочной раскладки визуализации
     *
    *************************************************************************************/

    //Продолжить объединение сообществ для создания древовидной раскладки


    //Отладочное сообщение
    #ifdef DEBUG
    mq_send(-3);
    mq_send(lnh_get_num(mQ));
    mq_send(lnh_get_num(cG));
    #endif

    //Удалить структуру двоичного дерева визуализации сообществ
    lnh_del_str_async(cvG);

    //Создание начального значения для индексных записей
    lnh_get_first(cG);
    do {

        //Получить номер сообщества
        com_u = (*(сom_u_key*)&lnh_core.result.key).__struct.comunity;
        adj_u = (*(сom_u_key*)&lnh_core.result.key).__struct.adj;


        //Получить информацию о вершинах сообщества u
        first_virtex_u=(*(сom_u_index*)&lnh_core.result.value).__struct.first_virtex;
        last_virtex_u=(*(сom_u_index*)&lnh_core.result.value).__struct.last_virtex;

        //Добавить индексные записи / Для записей сообществ листов дерева принят аналогичный cG формат индекса (сom_u_index), для остальных вершин дерева (сom_viz_index)
        lnh_ins_async(cvG,INLINE(сom_viz_key,{.adj=adj_u,.comunity=com_u}),INLINE(сom_u_index,{.first_virtex=first_virtex_u, .last_virtex=last_virtex_u}));

        v_count = 1;

        //Обойти все вершины в цепочке
        while (first_virtex_u!=last_virtex_u) {
     
            //Получить информацию о ребре Adj[i]
            lnh_search(G,INLINE(u_key,{.index=BASE_IDX,.u=first_virtex_u}));
            first_virtex_u = (*(u_attributes*)&lnh_core.result.value).__struct.pu;
            v_count++;

        }

        //Добавить индексные записи
        lnh_ins_async(cvG,INLINE(сom_viz_key,{.adj=CMTY_VCOUNT_IDX,.comunity=com_u}),INLINE(сom_viz_vcount,{.v_count=v_count, .is_leaf=true, .non=0}));
        lnh_ins_async(cvG,INLINE(сom_viz_key,{.adj=CMTY_XY_IDX,.comunity=com_u}),INLINE(сom_viz_xy,{.x0=0,.y0=0,.x1=0,.y1=0}));

    } while (lnh_ngr(cG,INLINE(сom_u_key,{.adj=CMTY_IDX,.comunity=com_u}))); //переход к следующему сообществу


    //Основной цикл
   while( (lnh_get_num(mQ)>2) && lnh_get_last_signed(mQ)) {
         
        //Получить номера объединяемых сообществ
        com_u = (*(q_comminities*)&lnh_core.result.value).__struct.com_v;
        com_v = (*(q_comminities*)&lnh_core.result.value).__struct.com_u;
        com_u_delta_mod = (*(q_modularity*)&lnh_core.result.key).__struct.delta_mod;
        com_u_index = (*(q_modularity*)&lnh_core.result.key).__struct.index;

        //Получить информацию об атрибутах связности u и v
        lnh_search(mQ,INLINE(q_modularity,{.index=com_u_index-1,.delta_mod=com_u_delta_mod}));
        w_u_v=(*(q_attributes*)&lnh_core.result.value).__struct.w_u_v;        

        //Удалить запись о модулярности связи сообществ u и v
        lnh_del_async(mQ,INLINE(q_modularity,{.index=com_u_index,.delta_mod=com_u_delta_mod})); 

        //Удалить запись с атрибутами
        lnh_del_async(mQ,INLINE(q_modularity,{.index=com_u_index-1,.delta_mod=com_u_delta_mod})); 

        //Удалить запись о связи сообществ u и v из структуры индексов
        lnh_del_async(iQ,INLINE(q_comminities,{.com_u=com_v,.com_v=com_u})); 

         //Найти запись о связи сообществ v и u в структуре индексов
        lnh_search(iQ,INLINE(q_comminities,{.com_u=com_u,.com_v=com_v})); 
        com_v_delta_mod = (*(q_modularity*)&lnh_core.result.value).__struct.delta_mod;
        com_v_index = (*(q_modularity*)&lnh_core.result.value).__struct.index;

        //Удалить запись о модулярности связи сообществ v и k
        lnh_del_async(mQ,INLINE(q_modularity,{.index=com_v_index,.delta_mod=com_v_delta_mod})); 

        //Удалить запись с атрибутами
        lnh_del_async(mQ,INLINE(q_modularity,{.index=com_v_index-1,.delta_mod=com_v_delta_mod})); 

        //Удалить запись о связи сообществ v и u из структуры индексов
        lnh_del_async(iQ,INLINE(q_comminities,{.com_u=com_u,.com_v=com_v})); 

        //Обновить модуляность - не является обязательным
        modularity+=com_u_delta_mod;

        //Получить информацию о вершинах сообщества v
        lnh_search(cvG,INLINE(сom_viz_key,{.adj=CMTY_VCOUNT_IDX,.comunity=com_v}));
        v_count=(*(сom_viz_vcount*)&lnh_core.result.value).__struct.v_count;
        lnh_nsm(cvG,INLINE(сom_viz_key,{.adj=CMTY_XY_IDX,.comunity=com_v}));
        adj_v=(*(сom_viz_key*)&lnh_core.result.key).__struct.adj;
    
        //Получить информацию о вершинах сообщества u
        lnh_search(cvG,INLINE(сom_viz_key,{.adj=CMTY_VCOUNT_IDX,.comunity=com_u}));
        v_count+=(*(сom_viz_vcount*)&lnh_core.result.value).__struct.v_count;
        lnh_nsm(cvG,INLINE(сom_viz_key,{.adj=CMTY_XY_IDX,.comunity=com_u}));
        adj_u=(*(сom_viz_key*)&lnh_core.result.key).__struct.adj;
        
        //Определить номер нового сообщества
        lnh_get_last(cvG);
        com_r=(*(сom_viz_key*)&lnh_core.result.key).__struct.comunity+1;
        lnh_ins_async(cvG,INLINE(сom_viz_key,{.adj=adj_u+adj_v-2,.comunity=com_r}),INLINE(сom_viz_index,{.left_leaf=com_u, .right_leaf=com_v}));
        lnh_ins_async(cvG,INLINE(сom_viz_key,{.adj=CMTY_VCOUNT_IDX,.comunity=com_r}),INLINE(сom_viz_vcount,{.v_count=v_count, .is_leaf=false, .non=0}));
        lnh_ins_async(cvG,INLINE(сom_viz_key,{.adj=CMTY_XY_IDX,.comunity=com_r}),INLINE(сom_viz_xy,{.x0=0,.y0=0,.x1=0,.y1=0}));

        //Отладочное сообщение
        #ifdef DEBUG
        mq_send(-4);
        mq_send(com_u);
        mq_send(com_v);
        mq_send(com_u_delta_mod);
        mq_send(modularity);
        mq_send(v_count);
        mq_send(com_r);
        #endif

        //Обойти все записи о сообществе u в очереди модулярности
        flag = lnh_nsm(iQ,INLINE(q_comminities,{.com_u=0,.com_v=com_u+1}));

        //Перебор структуры индексов смежных сообществ с сообществом com_v
        while (flag && (*(q_comminities*)&lnh_core.result.key).__struct.com_v == com_u) {

            //Получить индекс и модулярность из структуры инексов
            com_k = (*(q_comminities*)&lnh_core.result.key).__struct.com_u;
            com_k_index=(*(q_modularity*)&lnh_core.result.value).__struct.index;
            com_k_delta_mod=(*(q_modularity*)&lnh_core.result.value).__struct.delta_mod;

            //Для всех кандидатов, кроме com_u
            if (com_k!=com_v)  {

                //Получить атрибуты сообщества из очереди модулярности
                lnh_search(mQ,INLINE(q_modularity,{.index=com_k_index-1,.delta_mod=com_k_delta_mod}));
                w_k_v = (*(q_attributes*)&lnh_core.result.value).__struct.w_u_v; 

                //Получить информацию о кратности сообщества k
                lnh_nsm(cvG,INLINE(сom_viz_key,{.adj=CMTY_XY_IDX,.comunity=com_k}));
                adj_k=(*(сom_viz_key*)&lnh_core.result.key).__struct.adj;

                //Если сообщество com_k связано и с com_v тоже, то обновление уже произведено
                if (lnh_search(iQ,INLINE(q_comminities,{.com_u=com_v,.com_v=com_k}))) {

                    //Обновление уже произведено
                    //Do nothing

                } else {

                    //Если сообщество com_k не связано с com_v

                    //Удалить запись о модулярности связи сообществ u и k
                    lnh_del_async(mQ,INLINE(q_modularity,{.index=com_k_index,.delta_mod=com_k_delta_mod})); 

                    //Удалить запись с атрибутами
                    lnh_del_async(mQ,INLINE(q_modularity,{.index=com_k_index-1,.delta_mod=com_k_delta_mod})); 

                    //Удалить запись о связи сообществ u и k из структуры индексов
                    lnh_del_async(iQ,INLINE(q_comminities,{.com_u=com_k,.com_v=com_u})); 

                    //Сложить изменения модуляности (Aaron Clauset, M.E.J. Newman and Cristopher Moore. Finding community structure in very large networks, 10.c)
                    com_k_delta_mod = com_k_delta_mod - 2 * adj_u*adj_v*w_u_v;

                   //Определить новый номер в очереди за последним индексом для сегмента очереди com_k_delta_mod
                    if ( lnh_nsm(mQ,INLINE(q_modularity,{.index=0,.delta_mod=com_k_delta_mod+1})) && ((*(q_modularity*)&lnh_core.result.key).__struct.delta_mod == com_k_delta_mod) )
                        com_k_index = (*(q_modularity*)&lnh_core.result.key).__struct.index + 2; //Одна запись занимает два индекса
                    else
                        com_k_index = 1;                    

                    //Получить атрибуты связности сообществ k и u
                    lnh_search(iQ,INLINE(q_comminities,{.com_u=com_u,.com_v=com_k}));
                    delta_mod = (*(q_modularity*)&lnh_core.result.value).__struct.delta_mod; 
                    mq_index = (*(q_modularity*)&lnh_core.result.value).__struct.index; //Индекс записи о связи сообществ k и v

                    //Удалить запись о модулярности связи сообществ k и u
                    lnh_del_async(mQ,INLINE(q_modularity,{.index=mq_index,.delta_mod=delta_mod})); 

                    //Удалить запись с атрибутами
                    lnh_del_async(mQ,INLINE(q_modularity,{.index=mq_index-1,.delta_mod=delta_mod})); 

                    //Удалить запись о связи сообществ v и k из структуры индексов
                    lnh_del_async(iQ,INLINE(q_comminities,{.com_u=com_u,.com_v=com_k})); 

                    //Добавить запись об атрибутах связи сообществ k и r в очередь
                    lnh_ins_async(mQ,INLINE(q_modularity,{.index=com_k_index-1,.delta_mod=com_k_delta_mod}),INLINE(q_attributes,{.w_u_v=w_k_v,.non=0}));

                    //Добавить запись о модулярности  в следующий индекс
                    lnh_ins_async(mQ,INLINE(q_modularity,{.index=com_k_index,.delta_mod=com_k_delta_mod}),INLINE(q_comminities,{.com_u=com_r,.com_v=com_k}));

                    //Обновить запись в структуру индексов
                    lnh_ins_async(iQ,INLINE(q_comminities,{.com_u=com_r,.com_v=com_k}),INLINE(q_modularity,{.index=com_k_index,.delta_mod=com_k_delta_mod}));

                    com_k_index += 2; //Добавить обратную запись                 

                    //Добавить запись об атрибутах связи сообществ r и k в очередь
                    lnh_ins_async(mQ,INLINE(q_modularity,{.index=com_k_index-1,.delta_mod=com_k_delta_mod}),INLINE(q_attributes,{.w_u_v=w_k_v,.non=0}));

                    //Добавить запись о модулярности  в следующий индекс
                    lnh_ins_async(mQ,INLINE(q_modularity,{.index=com_k_index,.delta_mod=com_k_delta_mod}),INLINE(q_comminities,{.com_u=com_k,.com_v=com_r}));

                    //Добавить запись в структуру индексов
                    lnh_ins_async(iQ,INLINE(q_comminities,{.com_u=com_k,.com_v=com_r}),INLINE(q_modularity,{.index=com_k_index,.delta_mod=com_k_delta_mod}));

                }

            }

            //Перейти к следующему сообществу, связанному с com_v (перебор в обратном порядке)
            flag = lnh_nsm(iQ,INLINE(q_comminities,{.com_u=com_k,.com_v=com_u}));
        }

        //Обойти все записи о сообществе v в очереди модулярности
        flag = lnh_nsm(iQ,INLINE(q_comminities,{.com_u=0,.com_v=com_v+1}));

        //Перебор структуры индексов смежных сообществ с сообществом com_v
        while (flag && (*(q_comminities*)&lnh_core.result.key).__struct.com_v == com_v) {

            //Получить индекс и модулярность из структуры инексов
            com_k = (*(q_comminities*)&lnh_core.result.key).__struct.com_u;
            com_k_index=(*(q_modularity*)&lnh_core.result.value).__struct.index;
            com_k_delta_mod=(*(q_modularity*)&lnh_core.result.value).__struct.delta_mod;

            //Для всех кандидатов, кроме com_u
            if (com_k!=com_u)  {

                //Получить атрибуты сообщества из очереди модулярности
                lnh_search(mQ,INLINE(q_modularity,{.index=com_k_index-1,.delta_mod=com_k_delta_mod}));
                w_k_v = (*(q_attributes*)&lnh_core.result.value).__struct.w_u_v; 

                //Получить информацию о кратности сообщества k
                lnh_nsm(cvG,INLINE(сom_viz_key,{.adj=CMTY_XY_IDX,.comunity=com_k}));
                adj_k=(*(сom_viz_key*)&lnh_core.result.key).__struct.adj;

                //Если сообщество com_k связано и с com_u тоже
                if (lnh_search(iQ,INLINE(q_comminities,{.com_u=com_u,.com_v=com_k}))) {

                    //Получить атрибуты связности сообществ k и u
                    delta_mod = (*(q_modularity*)&lnh_core.result.value).__struct.delta_mod; 
                    mq_index = (*(q_modularity*)&lnh_core.result.value).__struct.index; //Индекс записи о связи сообществ k и u
                    lnh_search(mQ,INLINE(q_modularity,{.index=mq_index-1,.delta_mod=delta_mod}));
                    w_k_u = (*(q_attributes*)&lnh_core.result.value).__struct.w_u_v; 

                    //Удалить запись о модулярности связи сообществ v и k
                    lnh_del_async(mQ,INLINE(q_modularity,{.index=com_k_index,.delta_mod=com_k_delta_mod})); 

                    //Удалить запись с атрибутами
                    lnh_del_async(mQ,INLINE(q_modularity,{.index=com_k_index-1,.delta_mod=com_k_delta_mod})); 

                    //Удалить запись о связи сообществ k и v из структуры индексов
                    lnh_del_async(iQ,INLINE(q_comminities,{.com_u=com_k,.com_v=com_v})); 

                    //Удалить запись о модулярности связи сообществ k и u
                    lnh_del_async(mQ,INLINE(q_modularity,{.index=mq_index,.delta_mod=delta_mod})); 

                    //Удалить запись с атрибутами
                    lnh_del_async(mQ,INLINE(q_modularity,{.index=mq_index-1,.delta_mod=delta_mod})); 

                    //Удалить запись о связи сообществ k и u из структуры индексов
                    lnh_del_async(iQ,INLINE(q_comminities,{.com_u=com_u,.com_v=com_k})); 

                    //Сложить изменения модуляности (Aaron Clauset, M.E.J. Newman and Cristopher Moore. Finding community structure in very large networks, 10.a)
                    com_k_delta_mod = com_k_delta_mod + delta_mod;

                    //Получить атрибуты связности сообществ u и k
                    lnh_search(iQ,INLINE(q_comminities,{.com_u=com_k,.com_v=com_u}));
                    delta_mod = (*(q_modularity*)&lnh_core.result.value).__struct.delta_mod; 
                    mq_index = (*(q_modularity*)&lnh_core.result.value).__struct.index; //Индекс записи о связи сообществ u и k

                    //Удалить запись о модулярности связи сообществ u и k
                    lnh_del_async(mQ,INLINE(q_modularity,{.index=mq_index,.delta_mod=delta_mod})); 

                    //Удалить запись с атрибутами
                    lnh_del_async(mQ,INLINE(q_modularity,{.index=mq_index-1,.delta_mod=delta_mod}));                                        

                    //Удалить запись о связи сообществ u и k из структуры индексов
                    lnh_del_async(iQ,INLINE(q_comminities,{.com_u=com_k,.com_v=com_u})); 

                   //Определить новый номер в очереди за последним индексом для сегмента очереди com_k_delta_mod
                    if ( lnh_nsm(mQ,INLINE(q_modularity,{.index=0,.delta_mod=com_k_delta_mod+1})) && ((*(q_modularity*)&lnh_core.result.key).__struct.delta_mod == com_k_delta_mod) )
                        com_k_index = (*(q_modularity*)&lnh_core.result.key).__struct.index + 2; //Одна запись занимает два индекса
                    else
                        com_k_index = 1;                    

                    //Получить атрибуты связности сообществ k и v
                    lnh_search(iQ,INLINE(q_comminities,{.com_u=com_v,.com_v=com_k}));
                    delta_mod = (*(q_modularity*)&lnh_core.result.value).__struct.delta_mod; 
                    mq_index = (*(q_modularity*)&lnh_core.result.value).__struct.index; //Индекс записи о связи сообществ k и v

                    //Удалить запись о модулярности связи сообществ k и v
                    lnh_del_async(mQ,INLINE(q_modularity,{.index=mq_index,.delta_mod=delta_mod})); 

                    //Удалить запись с атрибутами
                    lnh_del_async(mQ,INLINE(q_modularity,{.index=mq_index-1,.delta_mod=delta_mod})); 

                    //Удалить запись о связи сообществ v и k из структуры индексов
                    lnh_del_async(iQ,INLINE(q_comminities,{.com_u=com_v,.com_v=com_k}));                 

                    //Добавить запись об атрибутах связи сообществ k и u в очередь
                    lnh_ins_async(mQ,INLINE(q_modularity,{.index=com_k_index-1,.delta_mod=com_k_delta_mod}),INLINE(q_attributes,{.w_u_v=w_k_u+w_k_v,.non=0}));

                    //Добавить запись о модулярности  в следующий индекс
                    lnh_ins_async(mQ,INLINE(q_modularity,{.index=com_k_index,.delta_mod=com_k_delta_mod}),INLINE(q_comminities,{.com_u=com_r,.com_v=com_k}));

                    //Изменить запись в структуру индексов (запись существует)
                    lnh_ins_async(iQ,INLINE(q_comminities,{.com_u=com_r,.com_v=com_k}),INLINE(q_modularity,{.index=com_k_index,.delta_mod=com_k_delta_mod}));

                    com_k_index += 2; //Добавить обратную запись                 

                    //Добавить запись об атрибутах связи сообществ u и k в очередь
                    lnh_ins_async(mQ,INLINE(q_modularity,{.index=com_k_index-1,.delta_mod=com_k_delta_mod}),INLINE(q_attributes,{.w_u_v=w_k_u+w_k_v,.non=0}));

                    //Добавить запись о модулярности  в следующий индекс
                    lnh_ins_async(mQ,INLINE(q_modularity,{.index=com_k_index,.delta_mod=com_k_delta_mod}),INLINE(q_comminities,{.com_u=com_k,.com_v=com_r}));

                    //Добавить запись в структуру индексов
                    lnh_ins_async(iQ,INLINE(q_comminities,{.com_u=com_k,.com_v=com_r}),INLINE(q_modularity,{.index=com_k_index,.delta_mod=com_k_delta_mod}));

                } else {

                    //Если сообщество com_k не связано с com_u

                    //Удалить запись о модулярности связи сообществ v и k
                    lnh_del_async(mQ,INLINE(q_modularity,{.index=com_k_index,.delta_mod=com_k_delta_mod})); 

                    //Удалить запись с атрибутами
                    lnh_del_async(mQ,INLINE(q_modularity,{.index=com_k_index-1,.delta_mod=com_k_delta_mod})); 

                    //Удалить запись о связи сообществ v и k из структуры индексов
                    lnh_del_async(iQ,INLINE(q_comminities,{.com_u=com_k,.com_v=com_v})); 


                    //Сложить изменения модуляности (Aaron Clauset, M.E.J. Newman and Cristopher Moore. Finding community structure in very large networks, 10.b)
                    com_k_delta_mod = com_k_delta_mod - 2 * adj_u*adj_v*w_u_v;


                   //Определить новый номер в очереди за последним индексом для сегмента очереди com_k_delta_mod
                    if ( lnh_nsm(mQ,INLINE(q_modularity,{.index=0,.delta_mod=com_k_delta_mod+1})) && ((*(q_modularity*)&lnh_core.result.key).__struct.delta_mod == com_k_delta_mod) )
                        com_k_index = (*(q_modularity*)&lnh_core.result.key).__struct.index + 2; //Одна запись занимает два индекса
                    else
                        com_k_index = 1;                    

                    //Получить атрибуты связности сообществ k и v
                    lnh_search(iQ,INLINE(q_comminities,{.com_u=com_v,.com_v=com_k}));
                    delta_mod = (*(q_modularity*)&lnh_core.result.value).__struct.delta_mod; 
                    mq_index = (*(q_modularity*)&lnh_core.result.value).__struct.index; //Индекс записи о связи сообществ k и v

                    //Удалить запись о модулярности связи сообществ k и v
                    lnh_del_async(mQ,INLINE(q_modularity,{.index=mq_index,.delta_mod=delta_mod})); 

                    //Удалить запись с атрибутами
                    lnh_del_async(mQ,INLINE(q_modularity,{.index=mq_index-1,.delta_mod=delta_mod})); 

                    //Удалить запись о связи сообществ k и v из структуры индексов
                    lnh_del_async(iQ,INLINE(q_comminities,{.com_u=com_v,.com_v=com_k}));                 

                    //Добавить запись об атрибутах связи сообществ k и u в очередь
                    lnh_ins_async(mQ,INLINE(q_modularity,{.index=com_k_index-1,.delta_mod=com_k_delta_mod}),INLINE(q_attributes,{.w_u_v=w_k_v,.non=0}));

                    //Добавить запись о модулярности  в следующий индекс
                    lnh_ins_async(mQ,INLINE(q_modularity,{.index=com_k_index,.delta_mod=com_k_delta_mod}),INLINE(q_comminities,{.com_u=com_r,.com_v=com_k}));

                    //Добавить запись в структуру индексов
                    lnh_ins_async(iQ,INLINE(q_comminities,{.com_u=com_r,.com_v=com_k}),INLINE(q_modularity,{.index=com_k_index,.delta_mod=com_k_delta_mod}));

                    com_k_index += 2; //Добавить обратную запись                 

                    //Добавить запись об атрибутах связи сообществ u и k в очередь
                    lnh_ins_async(mQ,INLINE(q_modularity,{.index=com_k_index-1,.delta_mod=com_k_delta_mod}),INLINE(q_attributes,{.w_u_v=w_k_v,.non=0}));

                    //Добавить запись о модулярности  в следующий индекс
                    lnh_ins_async(mQ,INLINE(q_modularity,{.index=com_k_index,.delta_mod=com_k_delta_mod}),INLINE(q_comminities,{.com_u=com_k,.com_v=com_r}));

                    //Добавить запись в структуру индексов
                    lnh_ins_async(iQ,INLINE(q_comminities,{.com_u=com_k,.com_v=com_r}),INLINE(q_modularity,{.index=com_k_index,.delta_mod=com_k_delta_mod}));


                }

            }

            //Перейти к следующему сообществу, связанному с com_v (перебор в обратном порядке)
            flag = lnh_nsm(iQ,INLINE(q_comminities,{.com_u=com_k,.com_v=com_v}));
        }

    }

    //Отладочное сообщение
    #ifdef DEBUG
    mq_send(0);
    #endif


    /**************************************************************************************
     * 
        IV - Рекурсивный обход дерева сообществ и определение границ прямоугольных областей
     *
    ***************************************************************************************/


    //Получить номер сообщества вершины дерева
    lnh_get_last(cvG);
    com_u=(*(сom_viz_key*)&lnh_core.result.key).__struct.comunity;
    //Вызов рекурсивной функции для определения границ прямоугодных областей
    set_xy(com_u,0,0,X_MAX,Y_MAX,true); 

    //удаление очереди модулярности
    lnh_del_str_async(mQ);
    
    //удаление структуры индексов
    lnh_del_str_async(iQ);

    //Отладочное сообщение
    #ifdef DEBUG
    mq_send(0);
    #endif


    /**************************************************************************************
     * 
        V - Обход вершин сообществ и формирование графа визуализации
     *
    ***************************************************************************************/


    //Удаление структуры визуализации
    lnh_del_str_async(vG);
    
    //Копирование графа G -> vG
    lnh_greq_sync(0ull,G,vG);

    unsigned short int x0,y0,x1,y1,x,y;
    unsigned int color;
    unsigned int new_btwc,btwc_min,btwc_sum,btwc_count = 0;
    unsigned int virtex_count = 0;
    unsigned short int x_mid;
    unsigned short int y_mid;
    unsigned short int size;
    unsigned int adj_c_max,adj_c_sum;
    unsigned short int a,b;
    unsigned int u;
    float size_factor;

    //Получить номер сообщества вершины дерева
    lnh_get_first(cvG);

    do {

       //Получить номер сообщества
        com_u = (*(сom_viz_key*)&lnh_core.result.key).__struct.comunity;
 
        //Получить информацию о вершинах сообщества u
        first_virtex_u=(*(сom_u_index*)&lnh_core.result.value).__struct.first_virtex;
        last_virtex_u=(*(сom_u_index*)&lnh_core.result.value).__struct.last_virtex;
        lnh_search(cvG,INLINE(сom_viz_key,{.adj=CMTY_VCOUNT_IDX,.comunity=com_u}));
        //Получить информацию о вершинах сообщества u
        v_count=(*(сom_viz_vcount*)&lnh_core.result.value).__struct.v_count;

        //Отладочное сообщение
        #ifdef DEBUG
        mq_send(-6);
        mq_send(com_u);
        mq_send(v_count);
        mq_send(first_virtex_u);
        mq_send(last_virtex_u);
        #endif
 
        //Получить информацию о границах визуализации сообщества com_u
        lnh_search(cvG,INLINE(сom_viz_key,{.adj=CMTY_XY_IDX,.comunity=com_u}));
        x0=(*(сom_viz_xy*)&lnh_core.result.value).__struct.x0;
        y0=(*(сom_viz_xy*)&lnh_core.result.value).__struct.y0;
        x1=(*(сom_viz_xy*)&lnh_core.result.value).__struct.x1;
        y1=(*(сom_viz_xy*)&lnh_core.result.value).__struct.y1;

        x_mid = x0+((x1-x0)>>1);
        y_mid = y0+((y1-y0)>>1);

        if ((x1-x0)>(y1-y0)) { 
            a = (x1-x0)>>1;
	    b = (y1-y0)>>1;
	}
        else {
            a = (y1-y0)>>1;
            b = (x1-x0)>>1;
	}

        //Очистка очереди
        lnh_del_str_async(Q);

        //Обойти все вершины в цепочке и создать очередь центральности
        adj_c_max=0;
        adj_c_sum=0;
        do {

            //Получить центральность
            lnh_search(G,INLINE(u_key,{.index=PTH_IDX,.u=first_virtex_u}));
            btwc = (*(u_index*)&lnh_core.result.value).__struct.btwc;

            //Запись очереди вершин
            lnh_ins_async(Q,INLINE(q_record,{.u=first_virtex_u,.index=btwc}),0);

            //Получить информацию о ребре Adj[i]
            lnh_search(G,INLINE(u_key,{.index=BASE_IDX,.u=first_virtex_u}));
            if (first_virtex_u!=last_virtex_u) 
                first_virtex_u = (*(u_attributes*)&lnh_core.result.value).__struct.pu; 
            else 
                break;
            adj_c_sum+=(*(u_attributes*)&lnh_core.result.value).__struct.adj_c;
            if (adj_c_max<(*(u_attributes*)&lnh_core.result.value).__struct.adj_c)
                adj_c_max=(*(u_attributes*)&lnh_core.result.value).__struct.adj_c;
        } while (true);

        virtex_count = lnh_get_num(Q);

        //Определить количество различных btwc и их сумму, количество adj и их сумму
        lnh_get_last(Q);
        btwc_sum=1;
        do {
            //Получение центральности
            btwc = (*(q_record*)&lnh_core.result.key).__struct.index;
            btwc_sum+=btwc;
        } while (lnh_nsm(Q,INLINE(q_record,{.u=0,.index=btwc}))); //переход к предыдущей вершине


        //Обойти все вершины в цепочке и получить значения x,y
        lnh_get_first(Q);
        btwc_min = (*(q_record*)&lnh_core.result.key).__struct.index;
        //Обойти все вершины в цепочке и получить значения x,y
        lnh_get_last(Q);
        //btwc_max = (*(q_record*)&lnh_core.result.key).__struct.index;

        //Определить коэффициент размеров вершин
        size_factor=float(b)/float(btwc_sum);
        //float size_factor=float(box_shortest_side>>1)/float(adj_c_sum);

        //Начальный радиус вершины с максимальным btwc
        int radius=0;
        size=0;
        btwc=0;
        new_btwc=0;

        do {
            //Перебор индексов вершин u
            u = (*(q_record*)&lnh_core.result.key).__struct.u;
            //Если btwc отличается от предыдущего, изменить радиус
            new_btwc=(*(q_record*)&lnh_core.result.key).__struct.index;
            if (new_btwc!=btwc)
                radius = radius + size/2 + size_factor*new_btwc/2;
            //Получение центральности
            btwc = new_btwc;
            //Размер вершины
            //lnh_search(vG,INLINE(u_key,{.index=BASE_IDX,.u=u}));
            //size = size_factor*(*(u_attributes*)&lnh_core.result.value).__struct.adj_c;
            size=size_factor*btwc;
            //Цвет вершины
            unsigned int intensity = (0xFF*btwc)/btwc_max;
            color=violet2rgb(intensity); //Light color
            //color=kelvin2rgb(intensity);

            //Угол положения на орбите
            float angle = float(rand_single()%628)/100.0;

            x = x_mid + radius * cos(angle);
            y = y_mid + radius * sin(angle);
            lnh_ins_async(vG,INLINE(u_key,{.index=VATR_IDX,.u=u}),INLINE(visualization_attributes,{.x=x,.y=y,.size=size,.color=color}));    

            //Отладочное сообщение
            #ifdef DEBUG
            mq_send(-7);
            mq_send(com_u);
            mq_send(u);
            mq_send(x);
            mq_send(y);
            mq_send(color);
            mq_send(size);
            mq_send(btwc);
            #endif

            //Радиус орбиты следующей вершины
            //radius+=size;

        } while (lnh_nsm(Q,INLINE(q_record,{.u=u,.index=btwc}))); //переход к предыдущей вершине


    } while (lnh_ngr(cvG,INLINE(u_key,{.index=IDX_MAX,.u=com_u})));


    //Отладочное сообщение
    #ifdef DEBUG
    mq_send(0);
    #endif


}




//-------------------------------------------------------------
// Получение информации о вершине
//-------------------------------------------------------------
/*
    Функция получает номер вершины и читает информацию из lnh64.
    Информация возвращается через очередь сообщений:
    @1 - Adj_c - количество ребер / 0 - если вершина не найдена
    @2 - Предыдущая вершина в кратчайшем пути
    @3 - Кратчайший путь
    @4 - Центральность
    @5 - Координата x
    @6 - Координата y
    @7 - Размер вершины size
    @8 - Цвет вершины color
    @9 - Для каждого ребра:
     @9.1 - Индекс вершины v
     @9.2 - Вес ребра w
*/
void get_virtex_data () {

    //Переменные
    unsigned int du,pu,btwc;
    short int adj_c,x,y;
    unsigned char size;
    unsigned int color;
    unsigned int u = mq_receive();
    //Чтение u из lnh64    
    if (lnh_search(vG,INLINE(u_key,{.index=BASE_IDX,.u=u}))) {
        adj_c = (*(u_attributes*)&lnh_core.result.value).__struct.adj_c;
        //adj_c
        mq_send(adj_c);
        //pu
        mq_send((*(u_attributes*)&lnh_core.result.value).__struct.pu);
        lnh_search(vG,INLINE(u_key,{.index=PTH_IDX,.u=u}));
        //du
        mq_send((*(u_index*)&lnh_core.result.value).__struct.du);
        //btwc
        mq_send((*(u_index*)&lnh_core.result.value).__struct.btwc);
        lnh_search(vG,INLINE(u_key,{.index=VATR_IDX,.u=u}));
        //x
        mq_send((*(visualization_attributes*)&lnh_core.result.value).__struct.x);
        //y
        mq_send((*(visualization_attributes*)&lnh_core.result.value).__struct.y);
        //size
        mq_send((*(visualization_attributes*)&lnh_core.result.value).__struct.size);
        //color
        mq_send( ((*(visualization_attributes*)&lnh_core.result.value).__struct.color <<8) | ALPHA_DEFAULT);
        for (unsigned int j=0; j<adj_c; j++) {
            lnh_search(vG,INLINE(u_key,{.index=j,.u=u}));
            //edge v
            mq_send( (*(edge*)&lnh_core.result.value).__struct.v );
            //edge w
            mq_send( (*(edge*)&lnh_core.result.value).__struct.w );
       }
    } else 
        mq_send(false);

}


//-------------------------------------------------------------
// Получение информации о первой вершине
//-------------------------------------------------------------
/*
    Функция получает номер вершины и и ищет следующую в lnh64.
    Информация возвращается через очередь сообщений:
    @1 - true - вершина найдена / false - вершина не найдена
    @2 - Индекс вершины
    @3 - Adj_c - количество ребер / 0 - если вершина не найдена
    @4 - Предыдущая вершина в кратчайшем пути
    @5 - Кратчайший путь
    @6 - Центральность
    @7 - Для каждого ребра:
    @7.1 - Индекс вершины v
    @7.2 - Вес ребра w
*/
void get_first_virtex () {

    if (lnh_get_first(vG)) {
        unsigned int u  = (*(u_key*)&lnh_core.result.key).__struct.u;
        mq_send(true);
        mq_send(u);
    } else 
        mq_send(false);

}


//-------------------------------------------------------------
// Получение информации о следующей вершине
//-------------------------------------------------------------
/*
    Функция получает номер вершины и и ищет следующую в lnh64.
    Информация возвращается через очередь сообщений:
    @1 - true - вершина найдена / false - вершина не найдена
    @2 - Индекс вершины
    @3 - Adj_c - количество ребер / 0 - если вершина не найдена
    @4 - Предыдущая вершина в кратчайшем пути
    @5 - Кратчайший путь
    @6 - Центральность
    @7 - Для каждого ребра:
    @7.1 - Индекс вершины v
    @7.2 - Вес ребра w
*/
void get_next_virtex () {

    //Переменные
    unsigned int u = mq_receive();
    //Поиск следующей вершины в  lnh64    
    if (lnh_ngr(vG,INLINE(u_key,{.index=IDX_MAX,.u=u}))) {
        unsigned int v  = (*(u_key*)&lnh_core.result.key).__struct.u;
        mq_send(true);
        mq_send(v);
    } else 
        mq_send(false);

}


//-------------------------------------------------------------
// Изменение атрибутов визуализации графа
//-------------------------------------------------------------
/*
    Функция изменяет атрибуты для указанной вершины
    Функция получает через очередь сообщений:
    Информация возвращается через очередь сообщений:
    @1 - Индекс вершины
    @2 - координату x
    @3 - координату y
    @4 - размер вершины
    @5 - цвет вершины
*/

void set_visualization_attributes () {

    //Переменные
    unsigned int u=mq_receive();
    unsigned short int x=mq_receive();
    unsigned short int y=mq_receive();
    unsigned short int size=mq_receive();
    unsigned int color=mq_receive();
    lnh_ins_async(vG,INLINE(u_key,{.index=VATR_IDX,.u=u}),INLINE(visualization_attributes,{.x=x,.y=y,.size=size,.color=color}));
}

//-------------------------------------------------------------
// Создание случайного графа
//-------------------------------------------------------------
/*
    Функция читает буфер buffer_pointer размером size байт
    Формат записи буфера: (u,v,w),(u,v,w),...,(u,v,w).
*/

void insert_edges () {

    //Переменные
    unsigned int u,v,du,pu,adj,dv,count=0;
    short int wu,adj_c;
    bool eQ,eQc;
    unsigned int i;
    //get buffer pointer and size
    unsigned int buffer_pointer = mq_receive();
    unsigned int size = mq_receive();
    //read u and v, then insert (u,v) into the graph    
    for (unsigned int i=buffer_pointer; i<buffer_pointer+size; i+=3*sizeof(int)) {
        unsigned int    u = *((volatile unsigned int*)(AXI4EXTMEM_BASE + i));
        unsigned int    v = *((volatile unsigned int*)(AXI4EXTMEM_BASE + i + sizeof(int))); 
        short int       w = *((volatile unsigned int*)(AXI4EXTMEM_BASE + i + 2*sizeof(int))); 
        //u->v edge
        if (lnh_search(G,INLINE(u_key,{.index=BASE_IDX,.u=u}))) {
            bool flag=false;
            adj_c = (*(u_attributes*)&lnh_core.result.value).__struct.adj_c;
            for (unsigned int j=0; j<adj_c; j++) {
                if ((lnh_search(G,INLINE(u_key,{.index=j,.u=u})) && ( *(edge*)&lnh_core.result.value).__struct.v == v) ) {
                    lnh_ins_async(G,INLINE(u_key,{.index=j, .u=u}),INLINE(edge,{.v=v, .w=w, .attr=0}));
                    flag=true;
                    break;
                }
            }
            if (!flag) {
                lnh_ins_async(G,INLINE(u_key,{.index=(unsigned int)adj_c, .u=u}),INLINE(edge,{.v=v, .w=w, .attr=0}));                   
                adj_c++;
                lnh_ins_async(G,INLINE(u_key,{.index=BASE_IDX, .u=u}),INLINE(u_attributes, {.pu=u, .eQ=true, .non=0, .adj_c=adj_c}));                   
                lnh_ins_async(G,INLINE(u_key,{.index=PTH_IDX, .u=u}),INLINE(u_index, {.du=INF,.btwc=0}));
            }
        } else {
                lnh_ins_async(G,INLINE(u_key,{.index=0, .u=u}),INLINE(edge,{.v=v, .w=w, .attr=0}));                   
                lnh_ins_async(G,INLINE(u_key,{.index=BASE_IDX, .u=u}),INLINE(u_attributes,{.pu=u, .eQ=true, .non=0, .adj_c=1}));                   
                lnh_ins_async(G,INLINE(u_key,{.index=PTH_IDX, .u=u}),INLINE(u_index,{.du=INF,.btwc=0}));
        }
    }
    lnh_sq_async(G);

}


//-------------------------------------------------------------
// Алгоритм Дейкстры
//-------------------------------------------------------------

void dijkstra_core(unsigned int start_virtex) {

    //Переменные
    unsigned int u,v,du,pu,adj,dv,btwc;
    short int wu,adj_c,count;
    bool eQ,eQc;
    unsigned int i;

    //Очистка очереди
    lnh_del_str_async(Q);
    //добавление стартовой вершины с du=0
    lnh_ins_async(Q,INLINE(q_record,{.u=start_virtex,.index=0}),0);
    //Get btwc to store it again 
    lnh_search(G,INLINE(u_key,{.index=PTH_IDX,.u=start_virtex}));
    btwc = (*(u_index*)&lnh_core.result.value).__struct.btwc;
    //Save du for start virtex 
    lnh_ins_async(G,INLINE(u_key,{.index=PTH_IDX,.u=start_virtex}),INLINE(u_index,{.du=0,.btwc=btwc}));
    //обход всех вершины графа   
    while (lnh_get_first(Q)) {
        //Search min in Q
        u = (*(q_record*)&lnh_core.result.key).__struct.u;
        du = (*(q_record*)&lnh_core.result.key).__struct.index;
        //Delete it from Q
        lnh_del_async(Q,lnh_core.result.key);
        //Get pu, |Adj|, eQ
        lnh_search(G,INLINE(u_key,{.index=BASE_IDX, .u=u}));
        pu = (*(u_attributes*)&lnh_core.result.value).__struct.pu;
        eQ = (*(u_attributes*)&lnh_core.result.value).__struct.eQ;
        adj_c = (*(u_attributes*)&lnh_core.result.value).__struct.adj_c; 
        // Clear eQ 
        lnh_ins_async(G,lnh_core.result.key,INLINE(u_attributes,{.pu=pu, .eQ=false, .non=0, .adj_c=adj_c}));
        //For each Adj
        for (i=0;i<adj_c;i++) {
            //Get Adj[i]
            lnh_search(G,INLINE(u_key,{.index=i,.u=u}));
            wu = (*(edge*)&lnh_core.result.value).__struct.w;
            adj = (*(edge*)&lnh_core.result.value).__struct.v;
            //Get information about Adj[i]
            lnh_search(G,INLINE(u_key,{.index=BASE_IDX,.u=adj}));
            eQc=(*(u_attributes*)&lnh_core.result.value).__struct.eQ;
            count=(*(u_attributes*)&lnh_core.result.value).__struct.adj_c;
            lnh_search(G,INLINE(u_key,{.index=PTH_IDX,.u=adj}));
            dv=(*(u_index*)&lnh_core.result.value).__struct.du;
            btwc=(*(u_index*)&lnh_core.result.value).__struct.btwc;
            //Change distance
            if (dv>(du+wu)) {
                if (eQc) {
                    //if not loopback, push to Q
                    if (dv!=INF) {
                        lnh_del_async(Q,INLINE(q_record,{.u=adj, .index=dv}));
                    }
                    lnh_ins_async(Q,INLINE(q_record,{.u=adj, .index=du+wu}),0);
                }
                //change du
                lnh_ins_async(G,INLINE(u_key,{.index=PTH_IDX,.u=adj}),INLINE(u_index,{.du=du+wu,.btwc=btwc}));
                //change pu
                lnh_ins_async(G,INLINE(u_key,{.index=BASE_IDX,.u=adj}),INLINE(u_attributes,{.pu=u, .eQ=eQc, .non=0, .adj_c=count}));
            }
        }
    }
}

void dijkstra () {

    //Создание стартовой вершины u=[0,0]
    unsigned int start_virtex = mq_receive(); //получаем номер старотовой вершины
    //Создание стоповой вершины v
    unsigned int stop_virtex = mq_receive(); //получаем номер стоповой вершины
    //Основной алгоритм
    dijkstra_core(start_virtex);
    //send shortest path
    lnh_search(G,INLINE(u_key,{.index=PTH_IDX, .u=stop_virtex}));
    mq_send((*(u_index*)&lnh_core.result.value).__struct.du);
}

//-------------------------------------------------------------
// Центральность
//-------------------------------------------------------------

void btwc () {

    //Переменные
    unsigned int u,v,pu,du,btwc;
    short int adj_c;

    //Select first virtex from Graph, assuming G is not empty and corretly inited
    lnh_get_first(G);
    do {

        //Iterate u
        u = (*(u_key*)&lnh_core.result.key).__struct.u;
        dijkstra_core(u);
        lnh_get_first(G);
        do {
            //Iterate v
            v = (*(u_key*)&lnh_core.result.key).__struct.u;
            //For undirected graphs needs to route 1/2 shortest paths (u<v)
            if (u!=v) { 
                lnh_search(G,INLINE(u_key,{.index=BASE_IDX, .u=v}));
                //Get pu
                pu = (*(u_attributes*)&lnh_core.result.value).__struct.pu;
                while (pu!=u) {
                   //Get btwc
                    lnh_search(G,INLINE(u_key,{.index=PTH_IDX,.u=pu}));
                    btwc = (*(u_index*)&lnh_core.result.value).__struct.btwc; 
                    du = (*(u_index*)&lnh_core.result.value).__struct.du;
                    //Write btwc, set du by the way
                    lnh_ins_async(G,INLINE(u_key,{.index=PTH_IDX,.u=pu}),INLINE(u_index,{.du=du,.btwc=btwc+1})); 
                    //Route shortest path 
                    lnh_search(G,INLINE(u_key,{.index=BASE_IDX, .u=pu}));
                    //Get next pu
                    pu = (*(u_attributes*)&lnh_core.result.value).__struct.pu;                
                }
            }
        } while (lnh_ngr(G,INLINE(u_key,{.index=IDX_MAX,.u=v}))); //Read next virtex

        //Init graph again
        lnh_get_first(G);
        do {
            //Iterate v
            v = (*(u_key*)&lnh_core.result.key).__struct.u;
            //Init graph again 
            lnh_search(G,INLINE(u_key,{.index=BASE_IDX, .u=v}));
            adj_c = (*(u_attributes*)&lnh_core.result.value).__struct.adj_c; 
            lnh_ins_async(G,INLINE(u_key,{.index=BASE_IDX, .u=v}),INLINE(u_attributes,{.pu=v, .eQ=true, .non=0, .adj_c=adj_c}));
            lnh_search(G,INLINE(u_key,{.index=PTH_IDX,.u=v}));
            btwc = (*(u_index*)&lnh_core.result.value).__struct.btwc; //(u->v) and (v->u)
            lnh_ins_async(G,INLINE(u_key,{.index=PTH_IDX, .u=v}),INLINE(u_index,{.du=INF,.btwc=btwc}));

        } while (lnh_ngr(G,INLINE(u_key,{.index=IDX_MAX,.u=v}))); //Read next virtex


    } while (lnh_ngr(G,INLINE(u_key,{.index=IDX_MAX,.u=u}))); //Read next virtex
}
