#ifndef DIJKSTRA_H_
#define DIJKSTRA_H_

#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include "lnh64.h"
#include "gpc_io_swk.h"


//Макросы для формирования структур inline 
#define STRUCT(type, ...) typedef union { struct __VA_ARGS__ __struct; uint64_t bits; } type
#define INLINE(type,...) (((type){__VA_ARGS__}).bits)


//Структуры для представления ключей и значений

//Структуры данных
#define 		G 	1 	//Граф
#define 		Q 	2 	//Очередь вершин
#define 		vG 	3 	//Граф визуализации
#define 		cG 	4 	//Структура сообществ
#define			mQ	5       //Очередь модулярности
#define			iQ	6       //Индексы очереди модулярности
#define 		cvG 	7 	//Структура вдоичного дерева для визуализации сообществ

//константы алгоритма
#define INF 0xFFFFFFFF 						//значение бесконечности для задания неинициализированного значения пути
#define ADJ_C_BITS 32						//количество бит для хранения индекса смежной вершины графа
const unsigned int 	IDX_MAX=(1ull<<ADJ_C_BITS)-1;		//максимальная смежность
#define PTH_IDX  	IDX_MAX 					//номер индексной записи о вершине
#define BASE_IDX 	IDX_MAX-1					//номер записи с атрибутами
#define VATR_IDX 	IDX_MAX-2					//атрибуты для визуализации
#define X_MAX    	(1<<12)*4/4					//размер поля визуализации по X
#define Y_MAX    	(1<<12)*4/8					//размер поля визуализации по Y
#define S_MAX    	(1<<8)/3					//максимальный размер вершины
#define ALPHA_DEFAULT	0xF0						//Альфа канал по умолчанию

///////////////////////////////////
// Граф 
///////////////////////////////////

//регистр ключа для вершины
	/* Struktura 1 - G - описание графа
	 * key[63..32] - номер вершины
	 * key[31..0] -  индекс записи о вершине (0,1..adj_u)
	 */
STRUCT( //Data structure for graph operations
u_key, {
        unsigned int                				index:32;	//Поле 0: индекс
        unsigned int                				u:32; 		//Поле 1: номер вершины
} );

//регистр значения индексной записи для вершины (с индексом PTH_IDX)
	 /* key[16..0] = PTH_IDX
	 * data[31..0] - d[u] - кратчайший путь
	 */ 
STRUCT( //Data structure for graph operations
u_index, {
        unsigned int                				du:32;    	//Поле 0: кратчайший путь
        unsigned int                				btwc:32;   	//Поле 1: центральность
} );

//регистр значения атрибутов для вершины с индексом BASE_IDX
	 /* для поля key[31..0] =  BASE_IDX
	 * data[31..0] - p[u] - pred nomer vershini v kratchajshem puti
	 * data[40..32] - 1 - u is in Q; 0 is not in Q
	 * data[63..48] - |Adj[u]| - kol-vo svjazej s vershinoj u
	 */
STRUCT( //Data structure for graph operations
u_attributes, {
		unsigned int					pu:32;		//Поле 0: номер предшествующей вершины в кратчайшем пути
		bool						eQ:8;		//Поле 1: флаг присутствия вершины в очереди Q
		bool						non:8;		//Поле 2: не используется
		short int  					adj_c:16;	//Поле 3: количество ребер вершины
} ); 

//регистр значения для записей о смежных вершинах
	/*
	 * key[INDEX] = 0..IDX_MAX-3
	 * data[15..0] - w[u,v] вес ребра
	 * data[47..16] - Adj[u]
	 * atr[63..48] - virtex atributes
	 */
STRUCT( //Data structure for graph operations
edge, {
		unsigned int					v:32;		//Поле 1: индекс смежной вершины				
		short int 					w:16; 		//Поле 0: вес ребра uv
		short int 					attr:16;	//Поле 2: атрибуты ребра
} ); 

///////////////////////////////////
// Очередь для алгоритма Дейкстры
///////////////////////////////////


//регистр ключа для записей очереди
	/*
	 * Struktura 2 - Q - ochered'
	 * key[31..0] - nomer vershini
	 * key[63..32] - d[u] kratchajshij put'
	*/
STRUCT( //Data structure for queue operations
q_record, {
		unsigned int					u:32;		//Поле 0: индекс вершины				
		unsigned int					index:32;	//Поле 1: кратчайший путь				
} ); 

///////////////////////////////////
// Запись сообщества
///////////////////////////////////

#define CMTY_IDX  	IDX_MAX					//номер индексной записи о вершине


//регистр ключа для вершины в графе сообществ	
	/* 
	 * key[63..32] - номер сообщества
	 * key[31..0] -  количество ребер сообщества (adj)
	 */

STRUCT( //Data structure for graph operations
сom_u_key, {
        unsigned int                				adj:32;		//Поле 0: кратность вершины сообщества
        unsigned int                				comunity:32; 	//Поле 1: номер сообщества
} );

//регистр значения индексной записи для вершины (с индексом CMTY_IDX)
	 /*
	 * data[31..0] - первая верина в цепочке вершин графа
	 * data[63..32] - последняя верина в цепочке вершин графа
	 */ 
STRUCT( //Data structure for graph operations
сom_u_index, {
	unsigned int  						first_virtex:32;	//Поле 0: количество вершин в сообществе
        unsigned int       					last_virtex:32;    	//Поле 1: не используется
} );


/////////////////////////////////////////
// Двоичное дерево визуализации сообществ
/////////////////////////////////////////

#define CMTY_VCOUNT_IDX  	IDX_MAX					//номер индексной записи о количестве вершин сообщества
#define CMTY_XY_IDX  		IDX_MAX-1				//номер индексной записи о границах области визуализации сообщества


//регистр ключа для вершины в графе сообществ
	/* 
	 * key[63..32] - номер сообщества
	 * key[31..0] -  количество ребер сообщества (adj)
	 */
STRUCT( //Data structure for graph operations
сom_viz_key, {
        unsigned int                				adj:32;		//Поле 0: кратность вершины сообщества
        unsigned int                				comunity:32; 	//Поле 1: номер сообщества
} );

//регистр значения индексной записи для вершины (с индексом Adj)
	 /* 
	 * data[31..0] - Номер левого сообщества в поддереве
	 * data[63..32] - Номер правого сообщества в поддереве
	 */ 
STRUCT( //Data structure for graph operations
сom_viz_index, {
	unsigned int  						left_leaf:32;	//Поле 0: Номер левого сообщества в поддереве
        unsigned int       					right_leaf:32;  //Поле 1: Номер правого сообщества в поддереве
} );


//регистр значения индексной записи для вершины (с индексом CMTY_VCOUNT_IDX)
	 /* сom_viz_u_key[31..0] = CMTY_VCOUNT_IDX, 
	 * data[31..0] - количество вершин в сообществе
	 * data[39..32] - флаг конечного листа двичного дерева сообществ
	 * data[63..32] - не используется

	 */ 
STRUCT( //Data structure for graph operations
сom_viz_vcount, {
	unsigned int  						v_count:32;	//Поле 0: количество вершин в сообществе
	bool	  						is_leaf:8;	//Поле 1: сообщество явлется конечным листов в двоичном дереве и не имеет потомков
	unsigned int						non:24;		//Поле 1: не используется
} );

//регистр значения индексной записи для вершины (с индексом CMTY_XY_IDX)
	 /* сom_viz_u_key[31..0] = CMTY_XY_IDX, 
	 * data[15..0] - левая верхняя координата x [0..4095]
	 * data[63..32] - не используется

	 */ 
STRUCT( //Data structure for graph operations
сom_viz_xy, {
	unsigned short int					x0:16;		//Поле 0: левая верхняя координата x [0..4095]
	unsigned short int					y0:16;		//Поле 1: левая верхняя координата y [0..4095]
	unsigned short int					x1:16;		//Поле 2: правая нижняя координата x [0..4095]
	unsigned short int					y1:16;		//Поле 3: правая нижняя координата y [0..4095] 
} );




////////////////////////////////////////////
// Очередь модулярности и структура индексов
////////////////////////////////////////////

//регистр ключа для очереди модулярности / регистр значения для структуры индексов
	/*
	 * Struktura 2 - Q - ochered'
	 * key[31..0] - индекс записи о модулярности
	 * key[63..32] - изменение модулярности при объединении сообществ u и v
	*/
STRUCT( //Data structure for queue operations
q_modularity, {
		unsigned int					index:32;	//Поле 0: индекс записи				
			 int					delta_mod:32;	//Поле 1: изменение модулярности				
} ); 

//регистр значения для очереди модулярности / регистр ключа для структуры индексов
	/*
	 * value[31..0] - номер сообщества u
	 * value[63..32] - номер сообщества v
	*/
STRUCT( //Data structure for queue operations
q_comminities, {
		unsigned int					com_u:32;	//Поле 0: номер сообщества u				
		unsigned int					com_v:32;	//Поле 1: номер сообщества v				
} ); 

//регистр атрибутов для очереди модулярности 
	/*
	 * value[31..0] - номер сообщества u
	 * value[63..32] - номер сообщества v
	*/
STRUCT( //Data structure for queue operations
q_attributes, {
		unsigned int					w_u_v:32;	//Поле 0: вес ребра uv				
		unsigned int					non:32;		//Поле 1: не используется				
} ); 


void dijkstra_core(unsigned int start_virtex);

///////////////////////////////////
// Визуализация графа
///////////////////////////////////

//регистр значения для записи атрибутов визуализации вершинах
	/*
	 * key[INDEX] = IDX_MAX-2
	 * data[11..0]  - координата x визуализации вершины
	 * data[23..12] - координата y визуализации вершины
	 * data[23..12] - Adj[u]
	 * atr[63..48] - virtex atributes
	 */

STRUCT( //Data structure for graph operations
visualization_attributes, {
		unsigned short int				x:12;		//Поле 1: координата x [0..4095]
		unsigned short int				y:12;		//Поле 2: координата y [0..4095]
		unsigned short int  			        size:8;		//Поле 3: размер [0..255]
		unsigned int					color:24;	//Поле 4: цвет [0x00000000..0xFFFFFFFF]
} ); 

#endif	

//Given a temperature (in Kelvin), estimate an RGB equivalent
const unsigned int kelvin_to_rgb[] = {     0xFF3800, 0xFF4700, 0xFF5300, 0xFF5D00, 0xFF6500,
	                                   0xFF6D00, 0xFF7300, 0xFF7900, 0xFF7E00, 0xFF8300,
	                                   0xFF8A12, 0xFF8E21, 0xFF932C, 0xFF9836, 0xFF9D3F,
	                                   0xFFA148, 0xFFA54F, 0xFFA957, 0xFFAD5E, 0xFFB165,
	                                   0xFFB46B, 0xFFB872, 0xFFBB78, 0xFFBE7E, 0xFFC184,
	                                   0xFFC489, 0xFFC78F, 0xFFC994, 0xFFCC99, 0xFFCE9F,
	                                   0xFFD1A3, 0xFFD3A8, 0xFFD5AD, 0xFFD7B1, 0xFFD9B6,
	                                   0xFFDBBA, 0xFFDDBE, 0xFFDFC2, 0xFFE1C6, 0xFFE3CA,
	                                   0xFFE4CE, 0xFFE6D2, 0xFFE8D5, 0xFFE9D9, 0xFFEBDC,
	                                   0xFFECE0, 0xFFEEE3, 0xFFEFE6, 0xFFF0E9, 0xFFF2EC,
	                                   0xFFF3EF, 0xFFF4F2, 0xFFF5F5, 0xFFF6F7, 0xFFF8FB,
	                                   0xFFF9FD, 0xFEF9FF, 0xFCF7FF, 0xF9F6FF, 0xF7F5FF,
	                                   0xF5F3FF, 0xF3F2FF, 0xF0F1FF, 0xEFF0FF, 0xEDEFFF,
	                                   0xEBEEFF, 0xE9EDFF, 0xE7ECFF, 0xE6EBFF, 0xE4EAFF,
	                                   0xE3E9FF, 0xE1E8FF, 0xE0E7FF, 0xDEE6FF, 0xDDE6FF,
	                                   0xDCE5FF, 0xDAE5FF, 0xD9E3FF, 0xD8E3FF, 0xD7E2FF,
	                                   0xD6E1FF, 0xD4E1FF, 0xD3E0FF, 0xD2DFFF, 0xD1DFFF,
	                                   0xD0DEFF, 0xCFDDFF, 0xCFDDFF, 0xCEDCFF, 0xCDDCFF,
	                                   0xCFDAFF, 0xCFDAFF, 0xCED9FF, 0xCDD9FF, 0xCCD8FF,
	                                   0xCCD8FF, 0xCBD7FF, 0xCAD7FF, 0xCAD6FF, 0xC9D6FF,
	                                   0xC8D5FF, 0xC8D5FF, 0xC7D4FF, 0xC6D4FF, 0xC6D4FF,
	                                   0xC5D3FF, 0xC5D3FF, 0xC5D2FF, 0xC4D2FF, 0xC3D2FF,
	                                   0xC3D1FF};
